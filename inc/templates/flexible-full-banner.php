<?php
use TFA\Helpers\Svg;
?>
<section class="full-banner">
    <div class="banner-logo">
        <?= Svg::inline('img/cabel_logo_w_deco.svg') ?>
    </div>

    <div class="banner-arrow">
        <?= Svg::inline('img/home_banner_arrow.svg') ?>
    </div>
</section>
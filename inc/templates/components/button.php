<?php // $content ?>

<?php $align = 'justify-content-';
switch ($content['align']){
    case 'left': $align .= 'start'; break;
    case 'right': $align .= 'end'; break;
    default: $align .= 'center'; break;
} ?>

<?php $class = 'btn'.(empty($content['style'])?'':'-'.$content['style']); ?>

<?php $href = empty($content['url'])?'':'href="'.$content['url'].'"'; ?>

<?php if(!empty($content['text'])): ?>
    <div class="btn-wrapper d-flex align-items-center <?= $class ?> <?= $align ?>">
        <a class="<?= $class ?>" <?= $href ?>>
            <?= $content['text'] ?>
        </a>
    </div>
<?php endif; ?>
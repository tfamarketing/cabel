<?php // $content ?>

<?php if($content['hasImage']||$content['hasContent']||$content['hasButton']): ?>
    <div class="basic-content">
        <?php 
        if($content['hasImage']) include_component('image', $content['image']); 
        if($content['hasContent']) include_component('wysiwyg', $content['content']); 
        if($content['hasButton']) include_component('button', $content['button']); 
        ?>
    </div>
<?php endif; ?>
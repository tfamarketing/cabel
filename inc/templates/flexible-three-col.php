<section class="three-col bg-<?= $backgroundColor ?>" <?= empty($backgroundImage['url'])?'':'style="background-image: url('.$backgroundImage['url'].')"'; ?>>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="column-top">
                    <?= $content ?>
                </div>
            </div>
            <div class="col-lg-7 col-12">
                <div class="content-left">
                    <?= $left['content'] ?>
                    <?php if($left['showDetails']): $info = get_field('information', 'options'); ?>
                        <div class="row icons">
                            <div class="col-auto row no-gutters align-items-center ">
                                <i class="fas fa-phone"></i>
                            </div>
                            <div class="col row no-gutters align-items-center">
                                <h6 class="txt-white">
                                    <b class="txt-green">WORKING HOURS:</b> <?= empty($info['telephone_1']['displayed'])?$info['telephone_1']['formatted']:$info['telephone_1']['displayed'] ?>
                                    <br/>
                                    <b class="txt-green">OUT OF HOURS:</b> <?= empty($info['telephone_1']['displayed'])?$info['telephone_1']['formatted']:$info['telephone_1']['displayed'] ?>
                                </h6>
                            </div>
                        </div>
                        <div class="row icons">
                            <div class="col-auto row no-gutters align-items-center ">
                                <i class="fas fa-envelope"></i>
                            </div>
                            <div class="col row no-gutters align-items-center">
                                <h6 class="txt-white"><b><?= $info['links']['email'] ?></b></h6> 
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-lg-5 col-12">
                <div class="content-right">
                    <?= $right['content'] ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
    $passwordProtectedForm = $passwordProtectedForm ?? false;
?>

<?php if (!empty($passwordProtectedForm)): ?>
    <div class="password-protected">
        <div class="container-fluid boxed">
            <?=$passwordProtectedForm?>
        </div>
    </div>
<?php endif; ?>
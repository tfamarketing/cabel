<?php if(!empty($content)): ?>

<section class="title-section bg-<?= $backgroundColor ?>" <?= empty($backgroundImage['url'])?'':'style="background-image: url('.$backgroundImage['url'].');"' ?>>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="content">
                    <?= $content ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php endif; ?>
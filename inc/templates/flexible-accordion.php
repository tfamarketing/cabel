<section class="accordion">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div id="accordion"> 
                    <?php foreach($items as $key => $item): ?>
                        <div class="item">
                            <h4 class="title collapsed" data-toggle="collapse" data-target="#item-<?= $key ?>" aria-expanded="true" aria-controls="item-<?= $key ?>">
                                <?= $item['title'] ?>
                                <span class="cross">
                                    <span></span><span></span>
                                </span>
                            </h4> 
                            <div id="item-<?= $key ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion"> 
                                <div class="content">
                                    <?= $item['content'] ?>
                                </div>
                            </div> 
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if(empty($caseStudies)) $caseStudies = new WP_Query([
    'posts_per_page' => -1,
    'post_type' => 'post',
]); ?>

<?php if($caseStudies->have_posts()): ?>
    <section class="case-studies bg-white">
        <div class="container">
            <div class="row">
                <?php while($caseStudies->have_posts()): $caseStudies->the_post(); $pid = get_the_ID(); ?>
                    <div class="col-lg-4 col-md-6 col-12 column">
                        <a href="<?= get_the_permalink($pid) ?>" class="case-study d-block">
                            <?php if(!empty(($imgurl = get_the_post_thumbnail_url($pid)))): ?>
                                <div class="image" style="background-image: url(<?= $imgurl ?>)"></div>
                            <?php endif; ?>
                            <div class="content position-relative">  
                                <h4><?= get_the_category($pid)[0]->name ?></h4>
                                <h2><?= get_the_title($pid) ?></h2>
                                <p><?= get_the_date('', $pid) ?></p>
                                <h6><span>Discover More</span></h6> 
                            </div>
                        </a>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
 
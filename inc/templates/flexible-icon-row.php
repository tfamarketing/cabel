<?php if($style === 'blue') $style = 'navygreen'; ?>

<section class="icon-row bg-<?= $style ?>">
    <div class="container">
        <div class="row">
            <?php if(!empty($title)): ?>
                <div class="col-lg-3 col-12">
                    <div class="title txt-center h-100 d-flex align-items-center justify-content-center">
                        <h3><?= $title ?></h3>
                    </div>
                </div>
            <?php endif; ?>
            <?php if(!empty($rows)): ?>
                <div class="<?= empty($title)?'col-12':'col-lg-9 col-12'; ?>">
                    <div class="icon-carousel<?= empty($title)?'':'-titled'; ?>">
                        <?php foreach($rows as $row): ?>
                            <div>
                                <div class="icon-wrapper">
                                    <img src="<?= $row['image']['url'] ?>" alt="<?= $row['image']['alt'] ?>">
                                    <h6><?= $row['text'] ?></h6>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
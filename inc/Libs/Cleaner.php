<?php

namespace TFA\Libs;

use DOMDocument;

/**
 * Cleaner for `InlineSvg\Svg()`
 */
class Cleaner
{
    /**
     * Execute the transformer.
     *
     * @param DOMDocument $dom
     */
    public function __invoke(DOMDocument $dom)
    {
        $prefix = uniqid();
        $elements = $dom->getElementsByTagName('*');

        //Remove <desc>
        foreach ($dom->getElementsByTagName('desc') as $element) {
            $element->parentNode->removeChild($element);
        }

        //Remove empty <defs>
        foreach ($dom->getElementsByTagName('defs') as $element) {
            if (!$element->hasChildNodes()) {
                $element->parentNode->removeChild($element);
            }
        }

        //Prefix all ids with unique prefix
        foreach ($elements as $element) {
            $id = $element->getAttribute('id');
            if (!empty($id)) {
                $element->setAttribute('id', "$prefix-$id");
            }
            $xLinkHref = $element->getAttribute('xlink:href');
            if ((!empty($xLinkHref)) && strpos($xLinkHref, '#') === 0) {
                $xLinkHref = substr($xLinkHref, 1);
                $element->setAttribute('xlink:href', "#$prefix-$xLinkHref");
            }
        }
    }
}

<?php

namespace TFA\Libs;

use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * @inheritDoc
 */
class Fields_Builder extends FieldsBuilder
{
    /**
     * @param bool $addAction Adds fields to action to register with ACF
     *
     * @inheritDoc
     */
    public function build(bool $addAction = true)
    {
        $fields = parent::build();
        if ($addAction) {
            add_action('acf/init', function () use ($fields) {
                acf_add_local_field_group($fields);
            });
        }
        return $fields;
    }
}

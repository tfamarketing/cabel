<?php

namespace TFA\Libs;

/**
 * Builds options page config array
 */
class Options_Page_Builder
{

    /**
     * Config array
     *
     * @var array
     */
    protected $config = [];

    public function __construct(string $name, string $label = '')
    {
        $this->updateConfig('menu_slug', $name);
        $name = (empty($label) ? $name : $label);
        $this->updateConfig('page_title', $this->generateLabel($name));
        $this->updateConfig('menu_title', $this->generateLabel($name));
    }

    /**
     * Returns array, then adds action to register page
     *
     * @param bool $addAction Adds fields to action to register with ACF
     *
     * @return array
     */
    public function build(bool $addAction = true)
    {
        if ($addAction && function_exists('acf_add_options_page')) {
            add_action('acf/init', function () {
                if (!$this->getConfig('parent_slug')) {
                    $this->config = acf_add_options_page($this->config);
                } else {
                    $this->config = acf_add_options_sub_page($this->config);
                }
            });
        }
        return $this->config;
    }

    /**
     * Sets description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description)
    {
        $this->updateConfig('description', __($description));
        return $this;
    }

    /**
     * Sets icon url
     *
     * @param string $icon
     * @return $this
     */
    public function setIconUrl(string $icon)
    {
        $this->updateConfig('icon_url', $icon);
        return $this;
    }

    /**
     * Sets redirect
     *
     * @param bool $redirect
     * @return $this
     */
    public function setRedirect(bool $redirect)
    {
        $this->updateConfig('redirect', $redirect);
        return $this;
    }

    /**
     * Sets post ID
     *
     * @param int|string $postId
     * @return $this
     */
    public function setPostId($postId)
    {
        $this->updateConfig('post_id', (int) $postId);
        return $this;
    }

    /**
     * Sets position
     *
     * @param bool $position
     * @return $this
     */
    public function setPosition(bool $position)
    {
        $this->updateConfig('position', $position);
        return $this;
    }

    /**
     * Sets autoload
     *
     * @param bool $autoload
     * @return $this
     */
    public function setAutoload(bool $autoload)
    {
        $this->updateConfig('autoload', $autoload);
        return $this;
    }

    /**
     * Sets update button text
     *
     * @param string $updateBtn
     * @return $this
     */
    public function setUpdateButton(string $updateBtn)
    {
        $this->updateConfig('update_button', $updateBtn);
        return $this;
    }

    /**
     * Sets update message
     *
     * @param string $updateMsg
     * @return $this
     */
    public function setUpdateMessage(string $updateMsg)
    {
        $this->updateConfig('update_message', $updateMsg);
        return $this;
    }

    /**
     * Sets parent slug
     *
     * @param string $slug
     * @return $this
     */
    public function setParentSlug(string $slug)
    {
        $this->updateConfig('parent_slug', $slug);
        return $this;
    }

    /**
     * Sets capability
     *
     * @param string $capability
     * @return $this
     */
    public function setCapability(string $capability)
    {
        $this->updateConfig('capability', $capability);
        return $this;
    }

    /**
     * Generates label from key
     *
     * @param string $name
     * @return $this
     */
    protected function generateLabel(string $name)
    {
        return __(ucwords(str_replace("_", " ", $name)));
    }

    /**
     * Updates config array with key/value
     *
     * @param string|int $key
     * @param mixed $value
     * @return void
     */
    protected function updateConfig($key, $value)
    {
        $this->config[$key] = $value;
    }

    /**
     * Gets key from config array
     *
     * @param string|int $key
     * @return mixed
     */
    public function getConfig($key)
    {
        return $this->config[$key] ?? null;
    }

}

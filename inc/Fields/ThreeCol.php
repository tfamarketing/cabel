<?php namespace TFA\Fields;

use TFA\Abstracts\Field;
use TFA\Libs\Fields_Builder; 

class ThreeCol extends Field
{
    public static function getName()
    {
        return 'threecol';
    } 
    
    public static function getSubFields()
    {
        return [
            Background::class
        ];
    }

    public static function callback(Fields_Builder $field, array $subFields)
    {
        $field  
            ->addFields(static::findField($subFields, 'background'))
            ->addWysiwyg('content')
            ->addGroup('left', ['wrapper' => ['width' => 50]])
                ->addWysiwyg('content')
                ->addTrueFalse('show_details', ['label' => 'Show Contact Information' ])
            ->endGroup()
            ->addGroup('right', ['wrapper' => ['width' => 50]])
                ->addWysiwyg('content') 
            ->endGroup();

        return $field;
    }
}
<?php

namespace TFA\Fields\Options;

use TFA\Abstracts\Field;
use TFA\Libs\Fields_Builder;

class Theme extends Field
{
    public static function getName() {
        return 'theme_options';
    }
}

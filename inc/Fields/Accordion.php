<?php

namespace TFA\Fields;

use TFA\Abstracts\Field;
use TFA\Libs\Fields_Builder;

class Accordion extends Field
{
    public static function getName()
    {
        return 'accordion';
    }

    public static function callback(Fields_Builder $field, array $subFields)
    {
        $field
            ->addRepeater('items')
                ->addText('title')
                ->addWysiwyg('content')
            ->endRepeater();

        return $field;
    }
}

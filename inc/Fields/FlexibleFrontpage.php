<?php namespace TFA\Fields;

use TFA\Abstracts\Field;
use TFA\Libs\Fields_Builder;  

class FlexibleFrontPage extends Field
{
    public static function getName()
    {
        return 'flexiblefrontpage';
    } 
    
    public static function getSubFields()
    {
        return [
            SimpleLayout::class,
            ThreeCol::class,
            FullBanner::class,
            ImageGrid::class
        ];
    }

    public static function callback(Fields_Builder $field, array $subFields)
    {
        $field
            ->addFlexibleContent('main_content', [
                'key' => 'main-content',
                'label' => 'Main Content' 
            ]) 
                // Flexible Fields
                ->addLayout('simple_layout')
                    ->addFields(static::findField($subFields, 'simplelayout'))
                ->addLayout('three_col')
                    ->addFields(static::findField($subFields, 'threecol')) 
                ->addLayout('full_banner')
                    ->addFields(static::findField($subFields, 'fullbanner')) 
                ->addLayout('image_grid')
                    ->addFields(static::findField($subFields, 'imagegrid'))
            ->endFlexibleContent()
            ->setLocation('post_template', '==', 'front-page.php');

        return $field;
    }
}
<?php namespace TFA\Fields;

use TFA\Abstracts\Field;
use TFA\Libs\Fields_Builder; 

class Image extends Field
{
    public static function getName()
    {
        return 'image';
    } 
    
    public static function getSubFields()
    {   
        
    }

    public static function callback(Fields_Builder $field, array $subFields)
    {
        $field 
            ->addGroup('image')
                ->addImage('img', ['wrapper' => ['width' => '40']])
                ->addSelect('align', [
                    'choices' => [
                        'left', 
                        'center',
                        'right' 
                    ],
                    'wrapper' => ['width' => '30']
                ])
                ->addSelect('style', [
                    'choices' => [
                        'none', 
                        'semi',
                        'full' 
                    ],
                    'wrapper' => ['width' => '30']
                ])
            ->endGroup();

        return $field;
    }
}
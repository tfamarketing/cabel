<?php namespace TFA\Fields;

use TFA\Abstracts\Field;
use TFA\Libs\Fields_Builder; 

class CaseStudies extends Field
{
    public static function getName()
    {
        return 'casestudies';
    } 
    
    public static function getSubFields()
    {
        
    }

    public static function callback(Fields_Builder $field, array $subFields)
    {
        $field   
        ->addPostObject('case_studies', [ 
            'multiple' => 1, 
            'post_type' => ['post'],
            'instructions' => 'Leaving this blank will result in all case studies being displayed'
        ]);

        return $field;
    }
}
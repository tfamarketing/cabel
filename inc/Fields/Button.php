<?php namespace TFA\Fields;

use TFA\Abstracts\Field;
use TFA\Libs\Fields_Builder; 

class Button extends Field
{
    public static function getName()
    {
        return 'button';
    } 
    
    public static function getSubFields()
    {
        
    }

    public static function callback(Fields_Builder $field, array $subFields)
    {
        $field
            ->addGroup('button')
                ->addText('text', ['wrapper' => ['width' => '30']])
                ->addLink('url', ['wrapper' => ['width' => '30']])
                ->addSelect('align', ['wrapper' => ['width' => '20'],
                    'choices' => [
                        'left', 
                        'center',
                        'right' 
                    ]
                ])
                ->addSelect('style', ['wrapper' => ['width' => '20'],
                    'choices' => [
                        'white', 
                        'green',
                        'navygreen'
                    ]
                ])
            ->endGroup();

        return $field;
    }
}
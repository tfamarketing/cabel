<?php namespace TFA\Fields;

use TFA\Abstracts\Field;
use TFA\Libs\Fields_Builder; 

class TitleSection extends Field
{
    public static function getName()
    {
        return 'titlesection';
    } 
    
    public static function getSubFields()
    {
        return [
            Background::class
        ];
    }

    public static function callback(Fields_Builder $field, array $subFields)
    {
        $field
            ->addWysiwyg('content')
            ->addFields(static::findField($subFields, 'background'));

        return $field;
    }
}
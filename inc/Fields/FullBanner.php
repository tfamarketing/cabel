<?php namespace TFA\Fields;

use TFA\Abstracts\Field;
use TFA\Libs\Fields_Builder; 

class FullBanner extends Field
{
    public static function getName()
    {
        return 'fullbanner';
    } 
    
    public static function getSubFields()
    {
        
    }

    public static function callback(Fields_Builder $field, array $subFields)
    {
        $field  
            ->addUrl('full_banner_video')
            ->addRepeater('slides')
                ->addImage('background_image') 
            ->endRepeater();

        return $field;
    }
}
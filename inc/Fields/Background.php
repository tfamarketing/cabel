<?php

namespace TFA\Fields;

use TFA\Abstracts\Field;
use TFA\Libs\Fields_Builder;

class Background extends Field
{
    public static function getName()
    {
        return 'background';
    }

    public static function callback(Fields_Builder $field, array $subFields)
    {
        $field
            ->addImage('background_image', ['wrapper' => ['width' => 60]])
            ->addSelect('background_color', ['wrapper' => ['width' => 40],
                'choices' => [
                    'white',
                    'grey',
                    'green',
                    'navygreen'
                ]
            ]);

        return $field;
    }
}

<?php namespace TFA\Fields;

use TFA\Abstracts\Field;
use TFA\Libs\Fields_Builder; 

class ImageGrid extends Field
{
    public static function getName()
    {
        return 'imagegrid';
    } 
    
    public static function getSubFields()
    {
        
    }

    public static function callback(Fields_Builder $field, array $subFields)
    {
        $field  
            ->addRepeater('grid_items', ['min' => 4, 'max' => 4, 'layout' => 'block',])
                ->addWysiwyg('title', ['wrapper' => ['width' => 35]])
                ->addLink('link', ['wrapper' => ['width' => 35]])
                ->addImage('image', ['preview_size' => 'icon', 'wrapper' => ['width' => 30] ])
                ->addAccordion('gallery-items')
                    ->addRepeater('gallery_items')
                        ->addText('title', ['wrapper' => ['width' => 60]])
                        ->addImage('image', ['preview_size' => 'icon', 'wrapper' => ['width' => 40] ])
                    ->endRepeater()
                ->addAccordion('gallery_items_end')->endpoint()
            ->endRepeater();

        return $field;
    }
}
<?php namespace TFA\Fields;

use TFA\Abstracts\Field;
use TFA\Libs\Fields_Builder; 

class BasicContent extends Field
{
    public static function getName()
    {
        return 'basiccontent';
    } 
    
    public static function getSubFields()
    {   
        return [
            Button::class,
            Image::class
        ];
    }

    public static function callback(Fields_Builder $field, array $subFields)
    {
        $field 
            ->addGroup('image')
                ->addImage('img', ['wrapper' => ['width' => '40']])
                ->addSelect('align', [
                    'choices' => [
                        'left', 
                        'center',
                        'right' 
                    ],
                    'wrapper' => ['width' => '30']
                ])
                ->addSelect('style', [
                    'choices' => [
                        'none', 
                        'semi',
                        'full' 
                    ],
                    'wrapper' => ['width' => '30']
                ])
            ->endGroup() 
            ->addWysiwyg('content') 
            ->addFields(static::findField($subFields, 'button'))
            
            ->addTrueFalse('has_image', ['wrapper'=>['width'=>33.3],'label' => 'Add Image Editor' ])
            ->addTrueFalse('has_content', ['wrapper'=>['width'=>33.3],'label' => 'Add Content Editor' ])
            ->addTrueFalse('has_button', ['wrapper'=>['width'=>33.3],'label' => 'Add Button Editor' ])
            ->getField('image')->conditional('has_image', '==', '1')
            ->getField('content')->conditional('has_content', '==', '1')
            ->getField('button')->conditional('has_button', '==', '1')
            ;

        return $field;
    }
}
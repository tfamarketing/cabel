<?php namespace TFA\Fields;

use TFA\Abstracts\Field;
use TFA\Libs\Fields_Builder;  

class Flexible extends Field
{
    public static function getName()
    {
        return 'flexible';
    } 
    
    public static function getSubFields()
    {
        return [
            Background::class,
            TitleSection::class,
            SimpleLayout::class,
            IconRow::class,
            CaseStudies::class,
            Banner::class,
            ContactForm::class,
            Accordion::class
        ];
    }

    public static function callback(Fields_Builder $field, array $subFields)
    {
        $field
            ->addFlexibleContent('main_content', [
                'key' => 'main-content',
                'label' => 'Main Content' 
            ]) 
                // Flexible Fields
                ->addLayout('banner')
                    ->addFields(static::findField($subFields, 'banner'))
                ->addLayout('title_section')
                    ->addFields(static::findField($subFields, 'titlesection'))
                ->addLayout('simple_layout')
                    ->addFields(static::findField($subFields, 'simplelayout'))
                ->addLayout('icon_row')
                    ->addFields(static::findField($subFields, 'iconrow'))
                ->addLayout('accordion')
                    ->addFields(static::findField($subFields, 'accordion'))
                ->addLayout('case_studies')
                    ->addFields(static::findField($subFields, 'casestudies'))
                ->addLayout('contact_form')
                    ->addFields(static::findField($subFields, 'contactform'))
                // Column Banner
                ->addLayout('column_banner')
                    ->addRepeater('columns', ['layout' => 'row'])
                        ->addFields(static::findField($subFields, 'background'))
                        ->addText('title')
                        // Column Content
                        ->addFlexibleContent('column_content', [
                            'key' => 'column-content',
                            'label' => 'Column Content' 
                        ]) 
                            // Flexible Fields
                            ->addLayout('title_section')
                                ->addFields(static::findField($subFields, 'titlesection'))
                            ->addLayout('simple_layout')
                                ->addFields(static::findField($subFields, 'simplelayout'))
                            ->addLayout('icon_row')
                                ->addFields(static::findField($subFields, 'iconrow'))
                        ->endFlexibleContent()
                    ->endRepeater()
            ->endFlexibleContent()
            ->setLocation('post_template', '==', 'page.php')
            ->or('post_template', '==', 'home.php')
            ->or('post_type', '==', 'post');

        return $field;
    }
}
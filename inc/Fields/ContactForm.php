<?php namespace TFA\Fields;

use TFA\Abstracts\Field;
use TFA\Libs\Fields_Builder; 

class ContactForm extends Field
{
    public static function getName()
    {
        return 'contactform';
    } 
    
    public static function getSubFields()
    {
        
    }

    public static function callback(Fields_Builder $field, array $subFields)
    {
        $field
            ->addWysiwyg('title')
            ->addText('shortcode')
            ->addTrueFalse('show_info', ['label' => 'Show Contact Information' ]);

        return $field;
    }
}
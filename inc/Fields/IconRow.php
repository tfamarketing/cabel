<?php namespace TFA\Fields;

use TFA\Abstracts\Field;
use TFA\Libs\Fields_Builder; 

class IconRow extends Field
{
    public static function getName()
    {
        return 'iconrow';
    } 
    
    public static function getSubFields()
    {
        
    }

    public static function callback(Fields_Builder $field, array $subFields)
    {
        $field   
            ->addText('title', ['wrapper' => ['width' => 75]])
            ->addSelect('style', ['choices' => [
                'blue', 
                'green'
            ], 'wrapper' => ['width' => 25]])
            ->addRepeater('rows')
                ->addImage('image', ['preview_size' => 'icon', 'wrapper' => ['width' => 25] ])
                ->addText('text', ['wrapper' => ['width' => 40]])
            ->endRepeater();

        return $field;
    }
}
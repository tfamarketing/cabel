<?php namespace TFA\Fields;

use TFA\Abstracts\Field;
use TFA\Libs\Fields_Builder; 

class Information extends Field
{
    public static function getName()
    {
        return 'information';
    }

    public static function callback(Fields_Builder $field, array $subFields)
    {
        $field
            ->addGroup('header')
                ->addText('text')
                ->addLink('link')
            ->endGroup()
            // Footer
            ->addGroup('footer') 
                ->addRepeater('images')
                    ->addImage('image')
                ->endRepeater()
            ->endGroup()
            // Information
            ->addGroup('information')
                ->addGroup('address')
                    ->addText('l1', ['wrapper' => ['width' => '50']])
                    ->addText('l2', ['wrapper' => ['width' => '50']])
                    ->addText('l3', ['wrapper' => ['width' => '50']])
                    ->addText('l4', ['wrapper' => ['width' => '50']])
                ->endGroup()
                ->addText('company_name') 
                ->addGroup('telephone_1', ['wrapper' => ['width' => '50']])
                    ->addText('displayed', ['wrapper' => ['width' => '50']])
                    ->addNumber('formatted', ['wrapper' => ['width' => '50']])
                ->endGroup()
                ->addGroup('telephone_2', ['wrapper' => ['width' => '50']])
                    ->addText('displayed', ['wrapper' => ['width' => '50']])
                    ->addNumber('formatted', ['wrapper' => ['width' => '50']])
                ->endGroup()
                ->addGroup('links')
                    ->addEmail('email', ['wrapper' => ['width' => '33.3']])
                    ->addUrl('facebook', ['wrapper' => ['width' => '33.3']])
                    ->addUrl('instagram', ['wrapper' => ['width' => '33.3']]) 
                ->endGroup()
                ->addGroup('open_hours')
                    ->addGroup('monday')
                        ->addTrueFalse('is_open', ['wrapper'=>['width'=>33.3],'label' => 'Is Open' ])
                        ->addTimePicker('open', ['wrapper'=>['width'=>33.3]])
                        ->addTimePicker('close', ['wrapper'=>['width'=>33.3]])
                    ->endGroup()
                    ->addGroup('tuesday')
                        ->addTrueFalse('is_open', ['wrapper'=>['width'=>33.3],'label' => 'Is Open' ])
                        ->addTimePicker('open', ['wrapper'=>['width'=>33.3]])
                        ->addTimePicker('close', ['wrapper'=>['width'=>33.3]])
                    ->endGroup()
                    ->addGroup('wednesday')
                        ->addTrueFalse('is_open', ['wrapper'=>['width'=>33.3],'label' => 'Is Open' ])
                        ->addTimePicker('open', ['wrapper'=>['width'=>33.3]])
                        ->addTimePicker('close', ['wrapper'=>['width'=>33.3]])
                    ->endGroup()
                    ->addGroup('thursday')
                        ->addTrueFalse('is_open', ['wrapper'=>['width'=>33.3],'label' => 'Is Open' ])
                        ->addTimePicker('open', ['wrapper'=>['width'=>33.3]])
                        ->addTimePicker('close', ['wrapper'=>['width'=>33.3]])
                    ->endGroup()
                    ->addGroup('friday')
                        ->addTrueFalse('is_open', ['wrapper'=>['width'=>33.3],'label' => 'Is Open' ])
                        ->addTimePicker('open', ['wrapper'=>['width'=>33.3]])
                        ->addTimePicker('close', ['wrapper'=>['width'=>33.3]])
                    ->endGroup()
                    ->addGroup('saturday')
                        ->addTrueFalse('is_open', ['wrapper'=>['width'=>33.3],'label' => 'Is Open' ])
                        ->addTimePicker('open', ['wrapper'=>['width'=>33.3]])
                        ->addTimePicker('close', ['wrapper'=>['width'=>33.3]])
                    ->endGroup()
                    ->addGroup('sunday')
                        ->addTrueFalse('is_open', ['wrapper'=>['width'=>33.3],'label' => 'Is Open' ])
                        ->addTimePicker('open', ['wrapper'=>['width'=>33.3]])
                        ->addTimePicker('close', ['wrapper'=>['width'=>33.3]])
                    ->endGroup()
                ->endGroup() 
            ->endGroup()
            ->setLocation('options_page', '==', 'information');

        return $field;
    }
}
<?php namespace TFA\Fields;

use TFA\Abstracts\Field;
use TFA\Libs\Fields_Builder; 

class SimpleLayout extends Field
{
    public static function getName()
    {
        return 'simplelayout';
    } 
    
    public static function getSubFields()
    {
        return [ 
            Background::class,
            BasicContent::class
        ];
    }

    public static function callback(Fields_Builder $field, array $subFields)
    {
        $field   
            ->addFields(static::findField($subFields, 'background'))
            ->addGroup('center_column', ['wrapper' => ['width' => 100]])
                ->addFields(static::findField($subFields, 'basiccontent'))
            ->endGroup()
            ->addGroup('left_column', ['wrapper' => ['width' => 50]])
                ->addFields(static::findField($subFields, 'basiccontent'))
            ->endGroup()
            ->addGroup('right_column', ['wrapper' => ['width' => 50]])
                ->addFields(static::findField($subFields, 'basiccontent'))
            ->endGroup();

        return $field;
    }
}
<?php namespace TFA\Fields;

use TFA\Abstracts\Field;
use TFA\Libs\Fields_Builder;  

class Banner extends Field
{
    public static function getName()
    {
        return 'banner';
    } 
    
    public static function getSubFields()
    {
        return [
            Background::class
        ];
    }

    public static function callback(Fields_Builder $field, array $subFields)
    {
        $field
            ->addFields(static::findField($subFields, 'background'))
            ->addImage('character')
            ->addText('title');

        return $field;
    }
}
<?php

namespace TFA\Abstracts;

use Auryn\Injector;
use TFA\Helpers\Generic;
use TFA\Interfaces\Static_Factory as Static_Factory_Interface;
use TFA\Traits\Prepare_Keys_For_Injector;

/**
 * Static factory base class
 */
abstract class Static_Factory implements Static_Factory_Interface
{
    use Prepare_Keys_For_Injector;

    /**
     * Class name
     *
     * @var string
     */
    public static $className = '';

    /**
     * @inheritDoc
     */
    public static function getInstance(array $args = [])
    {
        if ((static::$className = static::$className ?? get_called_class()) && class_exists(static::$className)) {
            $injector = new Injector;
            $args = static::prepareKeys($args);
            return $injector->make(static::$className, $args);
        }
    }
}

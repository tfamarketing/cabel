<?php

namespace TFA\Abstracts;

use TFA\Interfaces\Field as Field_Interface;
use TFA\Libs\Fields_Builder;

/**
 * Represents a config for the Stoutlogic Field Builder
 */
abstract class Field implements Field_Interface
{
    /**
     * @inheritDoc
     */
    public static function getName()
    {
        return 'field';
    }

    /**
     * @inheritDoc
     */
    public static function getArgs()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public static function getSubFields()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public static function callback(Fields_Builder $field, array $subFields)
    {
        return $field;
    }

    /**
     * @inheritDoc
     */
    public static function register()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public static function shareInstance()
    {
        return true;
    }

    /**
     * Finds field by key in array
     *
     * @param array $fields
     * @param string $key
     * @return Fields_Builder|array
     */
    protected static function findField(array $fields, string $key)
    {
        foreach ($fields as $field) {
            if ($field instanceof Fields_Builder && ($key == $field->getName())) {
                return $field;
            }
        }
        return [];
    }
}

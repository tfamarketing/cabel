<?php

namespace TFA\Abstracts;

use Auryn\Injector;
use TFA\Interfaces\Factory as Factory_Interface;
use TFA\Traits\Prepare_Keys_For_Injector;

/**
 * Factory base class
 */
abstract class Factory implements Factory_Interface
{
    use Prepare_Keys_For_Injector;

    /**
     * Class name
     *
     * @var string
     */
    public $className = '';

    /**
     * Injector instance
     *
     * @var Injector
     */
    protected $injector;

    public function __construct(Injector $injector)
    {
        $this->injector = $injector;
    }

    /**
     * @inheritDoc
     */
    public function getInstance(array $args = [])
    {
        if ($this->checkClassName()) {
            $args = (array) $this->getArgs($this->prepareKeys($args));
            return $this->injector->make($this->className, $args);
        }
    }

    /**
     * Checks class name is not empty & exists
     *
     * @return bool
     */
    protected function checkClassName() {
        return ($this->className ?? false) && class_exists($this->className);
    }

    /**
     * Returns arguments & editing of args from child class
     *
     * @param array $args
     * @return array
     */
    protected function getArgs(array $args)
    {
        return $args;
    }
}

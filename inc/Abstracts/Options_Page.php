<?php

namespace TFA\Abstracts;

use TFA\Interfaces\Options_Page as Options_Page_Interface;
use TFA\Libs\Options_Page_Builder;

/**
 * Represents a config for the Stoutlogic Field Builder
 */
abstract class Options_Page implements Options_Page_Interface
{
    /**
     * @inheritDoc
     */
    public static function getName()
    {
        return 'options';
    }

    /**
     * @inheritDoc
     */
    public static function callback(Options_Page_Builder $optionsPage)
    {
        return $optionsPage;
    }

    /**
     * @inheritDoc
     */
    public static function getParentPage()
    {
        return '';
    }
}

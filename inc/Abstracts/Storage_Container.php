<?php

namespace TFA\Abstracts;

use TFA\Interfaces\Container;
use TFA\Interfaces\Share;

/**
 * Storage container base class
 */
abstract class Storage_Container implements Share, Container
{
    /**
     * Storage array
     *
     * @var array
     */
    protected $storage = [];

    /**
     * @inheritDoc
     */
    public function shareInstance()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function add($id, $value)
    {
        if (!array_key_exists($id, $this->storage)) {
            $this->storage[$id] = $value;
        }
    }

    /**
     * @inheritDoc
     */
    public function get($id)
    {
        return array_key_exists($id, $this->storage) ? $this->storage[$id] : null;
    }

    /**
     * @inheritDoc
     */
    public function remove($id)
    {
        if (array_key_exists($id, $this->storage)) {
            unset($this->storage[$id]);
        }
    }

    /**
     * Returns storage array
     *
     * @return array
     */
    public function returnArray()
    {
        return $this->storage;
    }
}

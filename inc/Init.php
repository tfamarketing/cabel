<?php

namespace TFA;

use Auryn\Injector;
use HaydenPierce\ClassFinder\ClassFinder;
use TFA\Interfaces\Run;
use TFA\Interfaces\Share;

/**
 * Bootstrap class
 *
 * Initalizes services classes
 */
final class Init implements Run
{
    /**
     * Auryn injector instance
     *
     * @var Injector
     */
    protected $injector;

    /**
     * Array of global parameters
     *
     * @var array
     */
    protected $globalParams = [];

    /**
     * Classes to instantiate
     *
     * @var array
     */
    protected $classes = [];

    public function __construct(Injector $injector, array $globalParams)
    {
        $this->injector = $injector;
        $this->globalParams = $globalParams;
    }

    /**
     * @inheritDoc
     */
    public function run()
    {
        $this->setGlobalParams();
        $this->instantiateServices();
    }

    /**
     * Instantiates services
     *
     * @return void
     */
    protected function instantiateServices()
    {
        $this->classes = array_merge(
            ClassFinder::getClassesInNamespace('TFA\Storage', ClassFinder::RECURSIVE_MODE),
            ClassFinder::getClassesInNamespace('TFA\Setup', ClassFinder::RECURSIVE_MODE)
        );

        if ($this->classes ?? false) {
            foreach ($this->classes as $class) {
                if (is_a($class, Run::class, true)) {
                    $class = $this->injector->make($class);
                    $this->injector->execute([$class, 'run']);
                }
                if (is_a($class, Share::class, true)) {
                    $class = (is_object($class) ? $class : $this->injector->make($class));
                    if ($this->injector->execute([$class, 'shareInstance'])) {
                        $this->injector->share($class);
                    }
                }
            }
        }

        $this->injector->share($this->injector);
    }

    /**
     * Sets global params on the injector instance
     *
     * @return void
     */
    protected function setGlobalParams()
    {
        if ($this->globalParams ?? false) {
            foreach ($this->globalParams as $key => $value) {
                $this->injector->defineParam($key, $value);
            }
        }
    }
}

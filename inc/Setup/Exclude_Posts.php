<?php

namespace TFA\Setup;

use TFA\Factories\Fields_Builder as Fields_Builder_Factory;
use TFA\Helpers\Post;
use TFA\Interfaces\Run;
use TFA\Storage\Fields as Fields_Storage;

/**
 * Implements excluding posts from main loop
 */
class Exclude_Posts implements Run
{
    /**
     * Field storage container
     *
     * @var Fields_Storage
     */
    protected $fieldsStorage;

    /**
     * Fields builder factory
     *
     * @var Fields_Builder_Factory
     */
    protected $fieldsBuilderFactory;

    public function __construct(Fields_Storage $fieldsStorage, Fields_Builder_Factory $fieldsBuilderFactory)
    {
        $this->fieldsStorage = $fieldsStorage;
        $this->fieldsBuilderFactory = $fieldsBuilderFactory;
    }

    public function run()
    {
        $this->registerFields();
        add_filter('the_posts', [$this, 'modifyMainQuery'], 9998, 2);
    }

    /**
     * Modifies main query object to unset posts that have been marked for exclusion
     *
     * @param array $posts
     * @param WP_Query $query
     *
     * @return array
     */
    public function modifyMainQuery(array $posts, \WP_Query $query)
    {
        if (!empty($posts) && $query->is_archive() && (wp_doing_ajax() || !is_admin())) {
            foreach ($posts as $key => &$post) {
                $excludePost = get_field('exclude_post_from_archive', $post);
                if (($excludePost = $excludePost ?? false) && !empty($excludePost)) {
                    unset($posts[$key]);
                }
            }
            return array_values($posts);
        } else {
            remove_action('the_posts', [$this, 'modifyMainQuery'], 9999);
        }
        return $posts;
    }

    /**
     * Registers related ACF field groups
     *
     * @return void
     */
    public function registerFields()
    {
        $fields = $this->fieldsBuilderFactory->getInstance([
            'name' => 'exclude_post',
            'groupConfig' => [
                'position' => 'side',
            ],
        ]);
        $fields
            ->addTrueFalse('exclude_post_from_archive', [
                'label' => 'Exclude post from being shown on archive page',
            ])
            ->setLocation('post_type', '==', Post::getPostType());
        $this->fieldsStorage->add($fields->getName(), $fields);
        $fields->build();
    }
}

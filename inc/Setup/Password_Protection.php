<?php

namespace TFA\Setup;

use TFA\Helpers\Template;
use TFA\Interfaces\Run;
use TFA\Setup\Parent_Page_Redirection;

/**
 * Adds password protection support for posts
 */
class Password_Protection implements Run
{
    /**
     * Parent Page redirectionion instance
     *
     * @var Parent_Page_Redirection
     */
    protected $parentPageRedirection;

    /**
     * Current post object
     *
     * @var WP_Post|null
     */
    protected $currentPost = null;

    public function __construct(Parent_Page_Redirection $parentPageRedirection)
    {
        $this->parentPageRedirection = $parentPageRedirection;
    }

    /**
     * @inheritDoc
     */
    public function run()
    {
        add_action('template_redirect', [$this, 'redirection'], 5);
    }

    /**
     * Sets WP properties/variables for use in class
     *
     * @return void
     */
    public function setVars()
    {
        $this->currentPost = get_queried_object();
    }

    /**
     * Redirects post to a pseudo page template if password protection is required
     *
     * Hooked into: "template_redirect"
     *
     * Priority: 5
     *
     * @return void
     */
    public function redirection()
    {
        $this->setVars();
        if (!post_password_required($this->currentPost)) {
            remove_action('template_redirect', [$this, 'redirection'], 9);
        } else {
            if (!empty($passwordProtectedForm = get_the_password_form($this->currentPost))) {
                remove_action('template_redirect', [$this->parentPageRedirection, 'redirection'], 10);

                get_header();

                $templatePart = Template::getPart('templates/password-protected', compact('passwordProtectedForm'));

                if (!empty($templatePart)) {
                    echo $templatePart;
                } else {
                    echo $passwordProtectedForm;
                }

                get_footer();
            } else {
                remove_action('template_redirect', [$this, 'redirection'], 9);
            }
        }
    }
}

<?php

namespace TFA\Setup;

use TFA\Helpers\Path;
use TFA\Interfaces\Run;

/**
 * Registers theme assets
 */
class Scripts_Styles implements Run
{
    /**
     * Stylesheet dir
     *
     * @var string
     */
    protected $stylesheetDir;

    /**
     * Base version for assets
     *
     * @var string|int
     */
    protected $baseVersion;

    public function __construct($baseVersion)
    {
        $this->baseVersion = $baseVersion;
    }

    /**
     * @inheritDoc
     */
    public function run()
    {
        $this->setStylesheetDir();
        add_action('wp_loaded', [$this, 'register']);
        add_action('wp_enqueue_scripts', [$this, 'enqueue']);
    }

    /**
     * Register scripts and styles with `wp_register_script()` & `wp_register_styles()`
     *
     * Hooked into "wp_loaded"
     *
     * @return void
     */
    public function register()
    {
        //Scripts
        wp_register_script('tfa-manifest', $this->stylesheetDir . '/manifest.js', [], $this->getVersion());
        wp_register_script('tfa-vendor', $this->stylesheetDir . '/vendor.js', [], $this->getVersion());
        wp_register_script('tfa-scripts', $this->stylesheetDir . '/scripts.js', [], $this->getVersion());
        wp_register_script('tfa-load-more-posts', $this->stylesheetDir . '/load-more-posts.js', [], $this->getVersion());

        //Styles
        wp_register_style('fontawesome', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css', [], '', 'all');
        wp_register_style('tfa-main', $this->stylesheetDir . '/styles.css', [], $this->getVersion(), 'all');
    }

    /**
     * Enqueues scripts & styles with `wp_enqueue_script()` & `wp_enqueue_styles()`
     *
     * Hooked into: "wp_enqueue_scripts"
     *
     * @return void
     */
    public function enqueue()
    {
        //Scripts
        wp_enqueue_script('tfa-manifest');
        wp_enqueue_script('tfa-vendor');
        wp_enqueue_script('tfa-scripts');

        //Styles
        wp_enqueue_style('fontawesome');
        wp_enqueue_style('tfa-main');
    }

    /**
     * Saves stylesheet directory path using `get_stylesheet_directory_uri()`
     *
     * @return void
     */
    protected function setStylesheetDir()
    {
        $this->stylesheetDir = Path::getThemePath('dist', true);
    }

    /**
     * Returns static base version with sub version from param
     *
     * @param integer $subVersion
     * @return string
     */
    protected function getVersion($subVersion = 0): string
    {
        return $this->baseVersion . '.' . $subVersion;
    }
}

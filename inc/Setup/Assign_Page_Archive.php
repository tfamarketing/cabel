<?php

namespace TFA\Setup;

use TFA\Factories\Fields_Builder as Fields_Builder_Factory;
use TFA\Factories\Options_Page_Builder as Options_Page_Builder_Factory;
use TFA\Helpers\Generic;
use TFA\Helpers\Page;
use TFA\Helpers\WP;
use TFA\Interfaces\Run;
use TFA\Storage\Fields as Fields_Storage;

/**
 * Implements setting archive pages for custom post types
 */
class Assign_Page_Archive implements Run
{
    /**
     * Field storage container
     *
     * @var Fields_Storage
     */
    protected $fieldsStorage;

    /**
     * Fields builder factory
     *
     * @var Fields_Builder_Factory
     */
    protected $fieldsBuilderFactory;

    /**
     * Options page builder factory
     *
     * @var Options_Page_Builder_Factory
     */
    protected $optionsBuilderFactory;

    /**
     * Text domain
     *
     * @var string
     */
    protected $textDomain;

    /**
     * Name used throughout class
     *
     * Set by `setName()`
     *
     * @var string
     */
    protected $name = '';

    /**
     * Array of post type objects
     *
     * @var array
     */
    protected $postTypes = [];

    public function __construct(Fields_Storage $fieldsStorage, Options_Page_Builder_Factory $optionsBuilderFactory, Fields_Builder_Factory $fieldsBuilderFactory, $textDomain)
    {
        $this->fieldsStorage = $fieldsStorage;
        $this->fieldsBuilderFactory = $fieldsBuilderFactory;
        $this->optionsBuilderFactory = $optionsBuilderFactory;
        $this->textDomain = $textDomain;
    }

    /**
     * @inheritDoc
     */
    public function run()
    {
        $this->setName('assign_archive_page');
        add_action('init', function () {
            if ($this->getPostTypes()) {
                $this->registerFields();
                add_action('pre_get_posts', [$this, 'modifyMainQuery'], 11, 1);
            }
        });
        add_action('registered_post_type', [$this, 'modifyPostTypeObject'], 10, 2);
        add_action('admin_init', function () {
            if ($this->getPostTypes()) {
                add_action('current_screen', [$this, 'flushRewrites']);
                add_action('acf/save_post', [$this, 'setFlushFlag']);
                add_filter('display_post_states', [$this, 'addPostState'], 10, 2);
            }
        });
        add_filter('register_post_type_args', [$this, 'modifyPostTypeArgs'], 5, 2);
    }

    /**
     * Sets base name property in class
     *
     * @param string $name
     * @return void
     */
    public function setName(string $name)
    {
        $this->name = "{$this->textDomain}_{$name}";
    }

    /**
     * Gets post type objects that have archives
     *
     * @return array
     */
    public function getPostTypes()
    {
        $postTypesObjects = get_post_types([], 'objects');
        $postTypes = [];
        if (!empty($postTypesObjects)) {
            foreach ($postTypesObjects as $key => $postType) {
                if ($postType->has_archive) {
                    $postTypes[$key] = $postType;
                }
            }
        }
        return $this->postTypes = $postTypes;
    }

    /**
     * Modifies post type arguments to add slug
     *
     * Hooked into: "register_post_type_args"
     *
     * Priority: 5
     *
     * @param array $args
     * @param string $postType
     * @return array
     */
    public function modifyPostTypeArgs(array $args, string $postType)
    {
        if ($hasArchive = $args['has_archive'] ?? false) {
            $assignedPage = $this->getAssignedPage($postType, false);
            if (!empty($assignedPage) && ($slug = Page::getRelativePermalink($assignedPage))) {
                $args['rewrite'] = array_merge(compact('slug'), ['with_front' => false]);
            }
        }
        return $args;
    }

    /**
     * Modifies post type object to include the assigned archive page
     *
     * Hooked into: "registered_post_type"
     *
     * Priority: 10
     *
     * @return WP_Post_Type
     */
    public function modifyPostTypeObject(string $postType, \WP_Post_Type $postTypeObject)
    {
        if ($postTypeObject->has_archive) {
            $assignedPage = $this->getAssignedPage($postType);
            if (!empty($assignedPage = get_post($assignedPage))) {
                $postTypeObject->archive_page = $assignedPage;
                $postTypeObject->archive_page_id = (int) $assignedPage->ID;
            }
        }
        return $postTypeObject;
    }

    /**
     * Modifies main query object by setting queried object to assigned page
     *
     * Hooked into: "pre_get_posts"
     *
     * Priority: 11
     *
     * @param WP_Query $query
     * @return void
     */
    public function modifyMainQuery(\WP_Query $query)
    {
        $postType = ($query->query['post_type'] ?? false);
        if ($query->is_main_query() && $query->is_archive()) {
            if (!empty($postType)) {
                $postTypeObject = get_post_type_object($postType);
                $assignedPage = ($postTypeObject->archive_page ?? false);
                if ($assignedPage) {
                    $query->queried_object = $assignedPage;
                    $query->queried_object_id = (int) $assignedPage->ID;
                }
            }

            if (
                !is_admin() &&
                $archiveOptions = $this->getArchiveOptions($postType)
            ) {
                foreach ($archiveOptions as $key => $value) {
                    if (is_array($value)) {
                        $value = array_values(Generic::arrayFlatten($value));
                        if ($key == 'orderby') {
                            $value = implode(' ', $value);
                        }
                    }
                    $query->set($key, $value);
                }
            }
        }
    }

    /**
     * Registers related ACF options page & field groups
     *
     * @return void
     */
    public function registerFields()
    {
        if (function_exists('acf_add_options_page')) {
            $optionsPage = $this->optionsBuilderFactory->getInstance([
                'name' => "{$this->name}_options",
                'label' => 'Assign Page Archives',
            ]);
            acf_add_options_page($optionsPage->build(false));
            $fields = $this->fieldsBuilderFactory->getInstance([
                'name' => $this->name,
                'groupConfig' => [
                    'title' => 'Assign Pages',
                ],
            ]);
            foreach ($this->postTypes as $slug => $object) {
                $assignedPage = ($object->archive_page_id ?? false);
                $fields
                    ->addPostObject("{$this->name}_{$slug}", [
                        'label' => "{$object->label} Archive Page",
                        'post_type' => 'page',
                        'allow_null' => true,
                        'ui' => true,
                        'value' => $assignedPage,
                    ])
                    ->addGroup("{$this->name}_{$slug}_options", [
                        'label' => "{$object->label} Archive Page Options",
                    ])
                    ->addNumber('posts_per_page', [
                        'default_value' => (int) get_option('posts_per_page'),
                    ])
                    ->addSelect('order', [
                        'choices' => [
                            'ASC',
                            'DESC',
                        ],
                    ])
                    ->addRepeater('orderby')
                    ->addSelect('param', [
                        'choices' => [
                            'error',
                            'm',
                            'p',
                            'post_parent',
                            'subpost',
                            'subpost_id',
                            'attachment',
                            'attachment_id',
                            'name',
                            'pagename',
                            'page_id',
                            'second',
                            'minute',
                            'hour',
                            'day',
                            'monthnum',
                            'year',
                            'w',
                            'category_name',
                            'tag',
                            'cat',
                            'tag_id',
                            'author',
                            'author_name',
                            'feed',
                            'tb',
                            'paged',
                            'preview',
                            's',
                            'sentence',
                            'title',
                            'fields',
                            'menu_order',
                            'embed',
                        ],
                    ])
                    ->endRepeater()
                    ->endGroup();
            }
            $fields->setLocation('options_page', '==', "{$this->name}_options");
            $this->fieldsStorage->add($fields->getName(), $fields);
            acf_add_local_field_group($fields->build(false));
        }
    }

    /**
     * Adds post state to assigned pages
     *
     * Hooked into: "display_post_states"
     *
     * @param array $postStates Array of post states for the post
     * @param WP_Post $post Post object
     * @return array
     */
    public function addPostState(array $postStates, \WP_Post $post)
    {
        if (!empty($this->postTypes)) {
            foreach ($this->postTypes as $postType) {
                if (
                    !empty($postType->archive_page_id) &&
                    ($post->ID ?? 0) == $postType->archive_page_id &&
                    !empty($postType->labels->name)
                ) {
                    array_push($postStates, "{$postType->labels->name} Archive Page");
                    return $postStates;
                }
            }
        }
        return $postStates;
    }

    /**
     * Flushes permalinks based on conditional flag
     *
     * Hooked into: "current_screen"
     *
     * @return void
     */
    public function flushRewrites()
    {
        if ($this->isScreen()) {
            if (get_option("{$this->name}_flush_flag", false)) {
                flush_rewrite_rules();
                delete_option("{$this->name}_flush_flag");
            }
        }
    }

    /**
     * Sets flag for flushing of permalinks
     *
     * Hooked into: "acf/save_post"
     *
     * @return void
     */
    public function setFlushFlag()
    {
        if ($this->isScreen()) {
            add_option("{$this->name}_flush_flag", true);
        }
    }

    /**
     * Checks if current screen matches the options page
     *
     * @return bool
     */
    protected function isScreen()
    {
        $screen = get_current_screen();
        return (bool) (strpos($screen->id, "{$this->name}_options") == true);
    }

    /**
     * Gets assigned archive page for the post type
     *
     * @param WP_Post_Type|string $postType Post type related to page
     * @param bool $pageByPath Whether to search for page by rewrite string already present in post type args
     *
     * @return WP_Post|bool
     */
    protected function getAssignedPage($postType, bool $pageByPath = true)
    {
        $assignedPage = false;
        if (!empty($postType)) {
            $postTypeObj = ($postType instanceof \WP_Post_Type ? $postType : get_post_type_object($postType));

            $rewriteSlug = ($postTypeObj->rewrite['slug'] ?? '');
            $postTypeKey = (is_string($postType) ? $postType : ($postTypeObj->name ?? ''));

            $assignedPage = get_field("{$this->name}_{$postTypeKey}", 'option') ?: false;

            if (
                $pageByPath &&
                ($postTypeKey == 'product' &&
                    !WP::isPluginActive('woocommerce/woocommerce.php')) &&
                (empty($assignedPage) &&
                    (!empty($rewriteSlug) || $rewriteSlug != $postTypeKey))
            ) {
                $assignedPage = get_page_by_path($rewriteSlug) ?: false;
                $assignedPage = ($assignedPage->ID ?? '');
            }
            $assignedPage = is_numeric($assignedPage) ? (int) $assignedPage : $assignedPage;
        }
        return $assignedPage;
    }

    /**
     * Gets options related to archive options
     *
     * @param WP_Post_Type|string $postType Post type
     * @return array|bool
     */
    protected function getArchiveOptions($postType)
    {
        if (!empty($postType)) {
            $postTypeObj = ($postType instanceof \WP_Post_Type ? $postType : get_post_type_object($postType));
            $postTypeKey = (is_string($postType) ? $postType : ($postTypeObj->name ?? ''));
            return get_field("{$this->name}_{$postTypeKey}_options", 'option') ?: false;
        }
        return false;
    }
}

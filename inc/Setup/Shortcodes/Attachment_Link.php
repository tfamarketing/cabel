<?php

namespace TFA\Setup\Shortcodes;

use TFA\Helpers\Template;
use TFA\Interfaces\Run;

/**
 * Registers attachment link shortcode
 */
class Attachment_Link implements Run
{
    /**
     * @inheritDoc
     */
    public function run()
    {
        add_shortcode('tfa-attachment-link', [$this, 'attachmentLink']);
    }

    /**
     * Attachment link callback for shortcode
     *
     * Used with: `add_shortcode()`
     *
     * @param array $atts
     * @return string|bool
     */
    public function attachmentLink($atts, $content = null)
    {
        // Attributes
        $atts = shortcode_atts(
            [
                'id' => null,
                'name' => null,
                'target' => '_blank',
                'classes' => null
            ],
            $atts,
            'tfa-attachment-link'
        );

        if (!empty($atts['id'])) {
            $id = (int) $atts['id'];
        } elseif (!empty($atts['name'])) {
            $postObject = get_page_by_path((string) $atts['name']);
            $id = ($postObject instanceof \WP_Post ? $postObject->ID : false);
        } else {
            $id = false;
        }

        $target = $atts['target'] ?? false;

        if ($id) {
            $url = wp_get_attachment_url($id);
            $path = pathinfo(parse_url($url, PHP_URL_PATH));

            if (!empty($content)) {
                $fileNameWExt = trim($content);
            } elseif (!empty($fileName = $path['filename']) &&
                !empty($ext = $path['extension'])) {
                $fileNameWExt = "$fileName.$ext";
            } else {
                $fileNameWExt = false;
            }

            return trim(Template::getPart('templates/shortcodes/attachment-link', array_merge($atts, compact('id', 'url', 'fileNameWExt', 'target'))));
        }

        return '';
    }
}

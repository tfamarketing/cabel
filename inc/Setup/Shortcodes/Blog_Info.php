<?php

namespace TFA\Setup\Shortcodes;

use TFA\Interfaces\Run;

/**
 * Registers blog info shortcode
 */
class Blog_Info implements Run
{
    /**
     * @inheritDoc
     */
    public function run()
    {
        add_shortcode('tfa-blog-info', [$this, 'blogInfo']);
    }

    /**
     * Callback for shortcode
     *
     * Used with: `add_shortcode()`
     *
     * @param array $atts
     * @param string|null $content
     * @return string|bool
     */
    public function blogInfo($atts, $content = null)
    {
        // Attributes
        $atts = shortcode_atts(
            [
                'show' => 'name',
                'filter' => 'raw'
            ],
            $atts,
            'tfa-blog-info'
        );

        if (!empty($atts['show'])) {
            return get_bloginfo((string) $atts['show'], (string) (!empty($atts['filter']) ? $atts['filter'] : 'filter'));
        }

        return false;
    }
}

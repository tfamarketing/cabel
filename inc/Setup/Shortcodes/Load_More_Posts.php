<?php

namespace TFA\Setup\Shortcodes;

use TFA\Helpers\Generic;
use TFA\Helpers\Template;
use TFA\Helpers\WP;
use TFA\Interfaces\Run;

/**
 * Registers load more posts shortcode
 */
class Load_More_Posts implements Run
{
    protected $action = "tfa_load_more_posts_global";

    /**
     * @inheritDoc
     */
    public function run()
    {
        add_shortcode('tfa-load-more-posts', [$this, 'loadMorePosts']);
        WP::addActions([
            "wp_ajax_{$this->action}",
            "wp_ajax_nopriv_{$this->action}",
        ], [$this, 'ajaxRequest']);
    }

    /**
     * Button callback for shortcode
     *
     * Used with: `add_shortcode()`
     *
     * @param array $atts
     * @return string|bool
     */
    public function loadMorePosts($atts)
    {
        global $wp_query;

        if ($wp_query->max_num_pages > 1 && ($wp_query->is_archive || $wp_query->is_posts_page)) {
            // Attributes
            $atts = shortcode_atts(
                [
                    'template' => 'templates/partials/post',
                    'container-class' => '',
                    'result-class' => '',
                    'args' => json_encode([]),
                    'btn-more-text' => 'Load More',
                    'btn-less-text' => '',
                    'btn-class' => 'btn btn-primary',
                    'pre-load-text' => 'Loading more...',
                    'append-to-parent' => true,
                    'scroll-to-results' => true
                ],
                $atts,
                'tfa-load-more-posts'
            );

            $atts = Generic::changeCaseArrayKeys($atts, 'kebab', 'camel');
            $atts['args'] = Generic::getJsonArray($atts['args']);

            $atts = array_map(function ($value) {
                return !empty($value) ? ($value == 'true' ? true
                    : ($value == 'false' ? false : $value))
                    : (is_array($value) ? $value : false);
            }, $atts);

            wp_enqueue_script('tfa-load-more-posts');
            wp_localize_script('tfa-load-more-posts', $this->action, array_merge(
                compact('atts'),
                [
                    'ajaxUrl' => admin_url('admin-ajax.php'),
                    'posts' => $wp_query->query_vars,
                    'currentPage' => get_query_var('paged') ? get_query_var('paged') : 1,
                    'maxPage' => $wp_query->max_num_pages,
                ]
            ));

            return
                "<div id=\"{$this->action}\" class=\"tfa-load-more-posts {$atts['containerClass']}\">
                <div class=\"tfa-load-more-posts-result {$atts['resultClass']}\"></div>
                <button class=\"tfa-load-more-posts-btn {$atts['btnClass']}\">{$atts['btnMoreText']}</button>
            </div>";
        }

        return '';
    }

    /**
     * Ajax request that gets the posts
     *
     * Hooked into: "wp_ajax_{$this->action}", "wp_ajax_nopriv_{$this->action}"
     *
     * @return void
     */
    public function ajaxRequest()
    {
        global $wp_query;
        $args = $_POST['posts'] ?? [];
        $atts = $_POST['atts'] ?? [];
        $currentPage = ($_POST['currentPage'] ?? 1) + 1;

        $args = array_merge($args, ($atts['args'] ?? []), [
            'paged' => $currentPage
        ]);

        if (array_key_exists('nopaging', $args)) {
            unset($args['nopaging']);
        }

        query_posts($args);
        $wp_query->is_archive = true;

        if (have_posts()) {
            while (have_posts()) {
                the_post();
                echo Template::getPart($atts['template'] ?? '');
            }
        }

        wp_die();
    }
}

<?php

namespace TFA\Setup\Shortcodes;

use TFA\Helpers\Generic;
use TFA\Helpers\Template;
use TFA\Interfaces\Run;

/**
 * Registers button shortcode
 */
class Button implements Run
{
    /**
     * @inheritDoc
     */
    public function run()
    {
        add_shortcode('tfa-button', [$this, 'button']);
    }

    /**
     * Button callback for shortcode
     *
     * Used with: `add_shortcode()`
     *
     * @param array $atts
     * @param string|null $content
     * @return string|bool
     */
    public function button($atts, $content = null)
    {
        // Attributes
        $atts = shortcode_atts(
            [
                'full-width' => false,
                'bg-colour' => 'primary',
                'text-colour' => null,
                'url' => '#',
                'target' => '_self',
            ],
            $atts,
            'tfa-button'
        );

        $atts = Generic::changeCaseArrayKeys($atts, 'kebab', 'camel');

        if (!empty($content)) {
            return Template::getPart('templates/shortcodes/button', array_merge($atts, compact('content')));
        }

        return '';
    }
}

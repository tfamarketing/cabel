<?php

namespace TFA\Setup\Shortcodes\WooCommerce;

use TFA\Interfaces\Run;

/**
 * Registers WooCommerce custom register shortcode
 */
class Register implements Run
{
    /**
     * @inheritDoc
     */
    public function run()
    {
        add_shortcode('tfa-woocommerce-register', [$this, 'register']);
    }

    /**
     * Callback for shortcode
     *
     * Used with: `add_shortcode()`
     *
     * @return string|bool
     */
    public function register()
    {
        if (is_admin() || is_user_logged_in()) return false;
        wc_get_template('myaccount/form-register.php');
    }
}

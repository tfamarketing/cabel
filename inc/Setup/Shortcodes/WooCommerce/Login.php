<?php

namespace TFA\Setup\Shortcodes\WooCommerce;

use TFA\Interfaces\Run;

/**
 * Registers WooCommerce custom login shortcode
 */
class Login implements Run
{
    /**
     * @inheritDoc
     */
    public function run()
    {
        add_shortcode('tfa-woocommerce-login', [$this, 'login']);
    }

    /**
     * Callback for shortcode
     *
     * Used with: `add_shortcode()`
     *
     * @return string|bool
     */
    public function login($atts)
    {
        if (is_admin() || is_user_logged_in()) return false;
        // Attributes
        $atts = shortcode_atts(
            [
                'redirect' => '/accounts/my-account'
            ],
            $atts,
            'tfa-attachment-link'
        );

        ob_start();
        woocommerce_login_form(
            !empty($atts['redirect']) ? ['redirect' => home_url($atts['redirect'])] : []
        );
        return ob_get_clean() ?: false;
    }
}

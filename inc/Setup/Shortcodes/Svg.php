<?php

namespace TFA\Setup\Shortcodes;

use TFA\Helpers\Generic;
use TFA\Helpers\Svg as Svg_Helper;
use TFA\Interfaces\Run;

/**
 * Registers SVG shortcode
 */
class Svg implements Run
{
    /**
     * Array of svgs
     *
     * @var array
     */
    protected $svgs = [];

    /**
     * @inheritDoc
     */
    public function run()
    {
        $this->svgs = Generic::createArrayFromFiles('svg', false);
        add_shortcode('tfa-svg', [$this, 'svg']);
    }

    /**
     * Svg callback for shortcode
     *
     * Used with: `add_shortcode()`
     *
     * @param array $atts
     * @param string|null $content
     * @return string|bool
     */
    public function svg($atts)
    {
        // Attributes
        $atts = shortcode_atts(
            [
                'colour' => null,
                'name' => null,
                'max-width' => null,
                'html-tag' => 'span'
            ],
            $atts,
            'tfa-svg'
        );

        if (!empty($atts['name']) && array_key_exists($atts['name'], $this->svgs)) {
            $colour = (!empty($atts['colour']) ? [$atts['colour']] : []);
            $maxWidth = (!empty($atts['max-width']) ? ['max-width'] : []);
            $htmlTag = (!empty($atts['html-tag']) ? $atts['html-tag'] : 'span');

            $return = Svg_Helper::inline($this->svgs[$atts['name']], array_merge($colour, $maxWidth), false, $htmlTag);
            $return = Generic::stripLineBreaks($return);
            return $return;
        }

        return '';
    }
}

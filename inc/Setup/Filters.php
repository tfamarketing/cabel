<?php

namespace TFA\Setup;

use TFA\Interfaces\Run;
use TFA\Helpers\Page;

/**
 * Theme filters
 */
class Filters implements Run
{
    /**
     * Text domain
     *
     * @var string
     */
    protected $textDomain;

    public function __construct($textDomain)
    {
        $this->textDomain = $textDomain;
    }

    /**
     * @inheritDoc
     */
    public function run()
    {
        add_filter('wp_nav_menu_args', [$this, 'navMenuArgs']);
        add_filter('body_class', [$this, 'addSlugToBodyClass']);
        add_filter('excerpt_more', [$this, 'postExcerpt']);
        add_filter('the_content', [$this, 'removeEmptyParagraphTags'], 11);
    }

    /**
     * Modifies nav menu args to clean markup
     *
     * Hooked into: "wp_nav_menu_args"
     *
     * @param string $args
     * @return void
     */
    public function navMenuArgs($args = '')
    {
        $args['container'] = false;
        return $args;
    }

    /**
     * Adds post slug to body class
     *
     * Hooked into: "body_class"
     *
     * @return void
     */
    public function addSlugToBodyClass($classes)
    {
        global $post;
        if (is_home()) {
            $key = array_search('blog', $classes);
            if ($key > -1) {
                unset($classes[$key]);
            }
        } elseif (is_page()) {
            $classes[] = sanitize_html_class($post->post_name);
        } elseif (is_singular()) {
            $classes[] = sanitize_html_class($post->post_name);
        }

        return $classes;
    }

    /**
     * Filters post excerpts
     *
     * Hooked into "excerpt_more"
     *
     * @param string $more
     * @return string
     */
    public function postExcerpt(string $more = '')
    {
        global $post;
        return $more . '... <a class="view-article" href="' . get_permalink(get_the_ID($post)) . '">' . __('View Article', $this->textDomain) . '</a>';
    }

    /**
     * Removes empty p tags
     *
     * Hooked into "the_content"
     *
     * Priority: 11
     *
     * @see https://stackoverflow.com/questions/15472531/wordpress-empty-p-tags
     * @param string $content
     * @return string
     */
    public function removeEmptyParagraphTags(string $content)
    {
        $content = force_balance_tags($content);
        return preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
    }
}

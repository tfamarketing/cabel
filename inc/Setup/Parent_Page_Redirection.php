<?php

namespace TFA\Setup;

use TFA\Factories\Fields_Builder as Fields_Builder_Factory;
use TFA\Helpers\Page;
use TFA\Interfaces\Run;
use TFA\Interfaces\Share;
use TFA\Storage\Fields as Fields_Storage;

/**
 * Sets up page redirection for parent pages with children
 */
class Parent_Page_Redirection implements Run, Share
{
    /**
     * Fields builder factory instance
     *
     * @var Fields_Builder_Factory
     */
    protected $fieldsBuilderFactory;

    /**
     * Fields storage container instance
     *
     * @var Fields_Storage
     */
    protected $fieldsStorage;

    /**
     * Current post object
     *
     * @var WP_Post|null
     */
    protected $currentPost = null;

    /**
     * Array of `WP_Post()` objects that are children of the current post
     *
     * @var array
     */
    protected $pageChildren = [];

    public function __construct(Fields_Builder_Factory $fieldsBuilderFactory, Fields_Storage $fieldsStorage)
    {
        $this->fieldsBuilderFactory = $fieldsBuilderFactory;
        $this->fieldsStorage = $fieldsStorage;
    }

    /**
     * Runs/hooks functions depending on whether the user is in the admin panel or not
     *
     * @inheritDoc
     *
     * @return void
     */
    public function run()
    {
        if (is_admin()) {
            $this->setVars();
            $this->registerFields();
        } else {
            add_action('template_redirect', [$this, 'setVars'], 10);
            add_action('template_redirect', [$this, 'redirection'], 10);
        }
    }

    /**
     * Sets WP properties/variables for use in class
     *
     * @return void
     */
    public function setVars()
    {
        if (is_admin() && ($currentPost = $_GET['post'] ?? false)) {
            $this->currentPost = (int) $currentPost;
        } else {
            $this->currentPost = get_queried_object_id();
        }
        $this->pageChildren = Page::getChildren($this->currentPost, 1, true);
    }

    /**
     * Redirects page using field data
     *
     * Hooks into "template_redirect"
     *
     * Priority: 10
     *
     * @return void
     */
    public function redirection()
    {
        if ($this->currentPost && $this->pageChildren) {
            $redirectWhenLoggedIn = get_field('page_redirection_admin', $this->currentPost);
            if ((!is_user_logged_in()) || ($redirectWhenLoggedIn && is_user_logged_in())) {
                $enableRedirect = get_field('page_redirection_enable', $this->currentPost);

                if ($enableRedirect) {
                    $redirectPost = false;
                    $specificPost = get_field('page_redirection_post', $this->currentPost);
                    $specificPost = !empty($specificPost) && $specificPost != 'default' ?
                    (int) substr($specificPost, strlen('page-')) : false;

                    if ($specificPost && array_key_exists($specificPost, $this->pageChildren)) {
                        $redirectPost = $specificPost;
                    } else {
                        $redirectPost = reset($this->pageChildren);
                    }

                    if ($redirectPost && ($permalink = get_permalink($redirectPost))) {
                        wp_safe_redirect($permalink);
                        exit;
                    }
                } else {
                    remove_action('template_redirect', [$this, 'redirection'], 15);
                    remove_action('template_redirect', [$this, 'setVars'], 15);
                }
            } else {
                remove_action('template_redirect', [$this, 'redirection'], 15);
                remove_action('template_redirect', [$this, 'setVars'], 15);
            }
        } else {
            remove_action('template_redirect', [$this, 'redirection'], 15);
            remove_action('template_redirect', [$this, 'setVars'], 15);
        }
    }

    /**
     * Registers ACF fields for settings
     *
     * @return void
     */
    public function registerFields()
    {
        $fields = $this->fieldsBuilderFactory->getInstance([
            'name' => 'page_redirect',
        ]);
        $fields
            ->addTrueFalse('page_redirection_enable', [
                'label' => 'Redirect page to child',
            ])
            ->addTrueFalse('page_redirection_admin', [
                'label' => 'Enable page redirection when logged in',
                'default_value' => true,
            ])
            ->conditional('page_redirection_enable', '==', '1')
            ->addSelect('page_redirection_post', [
                'label' => 'Redirect to specific page',
                'choices' => $this->returnPageArrayWithNames($this->pageChildren),
                'ui' => true,
                'allow_null' => true,
            ])
            ->conditional('page_redirection_enable', '==', '1')
            ->setLocation('post_type', $this->pageChildren ? '==' : '!=', get_post_type($this->currentPost))
            ->and('post', '==', $this->pageChildren ? $this->currentPost : 0);
        $this->fieldsStorage->add($fields->getName(), $fields);
        $fields->build();
    }

    /**
     * Returns ACF select field friendly array of filtered post objects
     *
     * @param array $pages
     * @return array
     */
    protected function returnPageArrayWithNames(array &$pages)
    {
        foreach ($pages as $key => $page) {
            if ($page instanceof \WP_Post) {
                $pages["page-{$page->ID}"] = $page->post_title;
            }
            unset($pages[$key]);
        }
        return $pages;
    }

    public function shareInstance() {
        return true;
    }
}

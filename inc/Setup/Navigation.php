<?php

namespace TFA\Setup;

use TFA\Interfaces\Run;
use TFA\Helpers\Page;

/**
 * Navigation
 */
class Navigation implements Run
{
    /**
     * Text domain
     *
     * @var string
     */
    protected $textDomain;

    public function __construct($textDomain)
    {
        $this->textDomain = $textDomain;
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        add_action('after_setup_theme', [$this, 'registerMenus']);
        add_filter('nav_menu_css_class', [$this, 'addDirectAncestorPageClass'], 10, 2);
    }

    /**
     * Registers nav menus with WordPress using `register_nav_menus()`
     *
     * Hooked into: "after_setup_theme"
     *
     * @return void
     */
    public function registerMenus()
    {
        register_nav_menus(
            [
                'tfa-main-menu' => __('Main Menu', $this->textDomain), // Main Navigation
                'tfa-footer-menu-1' => __('Footer Menu 1', $this->textDomain), // Footer Navigation
                'tfa-footer-menu-2' => __('Footer Menu 2', $this->textDomain), // Footer Navigation
            ]
        );
    }

    /**
     * Adds direct ancestor class to nav menu items
     *
     * @param array $classes Array of nav menu item classes
     * @param WP_Post $item Nav menu item post object
     *
     * Hooked into: "nav_menu_css_class"
     *
     * @return array
     */
    public function addDirectAncestorPageClass(array $classes, \WP_Post $item)
    {
        if (
            !empty($item->object_id) &&
            !empty($item->object) &&
            $item->object != 'nav_menu_item' &&
            (!empty($children = Page::getChildren($item->object_id, 1, true)))
        ) {
            $children = array_map(function ($child) {
                return ($child->ID ?? 0);
            }, $children);

            if ((get_queried_object_id() ?: 0) == reset($children)) {
                $classes[] = 'current-page-direct-ancestor';
            }
        }

        return $classes;
    }
}

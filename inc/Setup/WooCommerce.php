<?php

namespace TFA\Setup;

use TFA\Interfaces\Run;

class WooCommerce implements Run
{

    public function run()
    {
        add_filter('woocommerce_form_field_args', [$this, 'inputBootstrapClasses']);
        add_filter('woocommerce_style_smallscreen_breakpoint', [$this, 'changeBreakpoint']);
    }

    /**
     * Change breakpoint of stylesheets
     *
     * Hooked into: "woocommerce_style_smallscreen_breakpoint"
     *
     * @return void
     */
    public function changeBreakpoint() {
        return '767.98px';
    }

    /**
     * Adds bootstrap classes to WooCommerce fields
     *
     * Hooked into: "woocommerce_form_field_args"
     *
     * @param array $args
     * @return array
     */
    public function inputBootstrapClasses(array $args)
    {
        switch ($args['type']) {
            case 'select':
                $args['class'][] = 'form-group';
                $args['input_class'] = ['form-control', 'input-lg'];
                $args['label_class'] = ['control-label'];
                $args['custom_attributes'] = [
                    'data-plugin' => 'select2',
                    'data-allow-clear' => 'true',
                    'aria-hidden' => 'true',
                ];
                break;
            case 'country':
                $args['class'][] = 'form-group single-country';
                $args['label_class'] = ['control-label'];
                break;
            case 'state':
                $args['class'][] = 'form-group';
                $args['input_class'] = ['form-control', 'input-lg'];
                $args['label_class'] = ['control-label'];
                $args['custom_attributes'] = [
                    'data-plugin' => 'select2',
                    'data-allow-clear' => 'true',
                    'aria-hidden' => 'true',
                ];
                break;
            case 'password':
            case 'text':
            case 'email':
            case 'tel':
            case 'number':
                $args['class'][] = 'form-group';
                $args['input_class'] = ['form-control', 'input-lg'];
                $args['label_class'] = ['control-label'];
                break;
            case 'textarea':
                $args['input_class'] = ['form-control', 'input-lg'];
                $args['label_class'] = ['control-label'];
                break;
            case 'checkbox':
                $args['label_class'] = ['custom-control custom-checkbox'];
                $args['input_class'] = ['custom-control-input', 'input-lg'];
                break;
            case 'radio':
                $args['label_class'] = ['custom-control custom-radio'];
                $args['input_class'] = ['custom-control-input', 'input-lg'];
                break;
            default:
                $args['class'][] = 'form-group';
                $args['input_class'] = ['form-control', 'input-lg'];
                $args['label_class'] = ['control-label'];
                break;
        }
        return $args;
    }
}

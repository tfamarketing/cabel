<?php

namespace TFA\Setup;

use TFA\Interfaces\Run;

/**
 * Adds theme supports
 */
class Theme_Supports implements Run
{
    /**
     * Text domain
     *
     * @var string
     */
    protected $textDomain;

    public function __construct($textDomain)
    {
        $this->textDomain = $textDomain;
    }

    /**
     * @inheritDoc
     */
    public function run()
    {
        add_action('after_setup_theme', [$this, 'themeSupports']);
        add_action('after_setup_theme', [$this, 'imageSizes']);
        add_action('after_setup_theme', [$this, 'textDomain']);
        add_action('init', [$this, 'postTypes']);
    }

    /**
     * Add themes supports using `add_theme_support()`
     *
     * Hooked into: "after_setup_theme"
     *
     * @return void
     */
    public function themeSupports()
    {
        add_theme_support('menus');
        add_theme_support('automatic-feed-links');
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
        add_theme_support('custom-logo');
        add_theme_support('woocommerce');
    }

    /**
     * Add image sizes using `add_image_size()`
     *
     * Hooked into: "after_theme_setup"
     *
     * @return void
     */
    public function imageSizes()
    {
        add_image_size('large', 700, '', true);
        add_image_size('medium', 250, '', true);
        add_image_size('small', 120, '', true);
        add_image_size('custom-size', 700, 200, true);
    }

    /**
     * Adds post type supports to existing post types
     *
     *
     * @return void
     */
    public function postTypes()
    {
        add_post_type_support('page', 'excerpt');
    }

    /**
     * Adds text domain via `load_theme_textdomain()`
     *
     * Hooked into: "after_theme_setup"
     *
     * @return void
     */
    public function textDomain()
    {
        load_theme_textdomain($this->textDomain, get_stylesheet_directory() . '/languages');
    }
}

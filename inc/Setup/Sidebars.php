<?php

namespace TFA\Setup;

use TFA\Interfaces\Run;

/**
 * Registers sidebars
 */
class Sidebars implements Run
{
    /**
     * Text domain
     *
     * @var string
     */
    protected $textDomain;

    public function __construct($textDomain)
    {
        $this->textDomain = $textDomain;
    }

    /**
     * @inheritDoc
     */
    public function run()
    {
        add_action('after_setup_theme', [$this, 'register']);
    }

    /**
     * Registers sidebars using `register_sidebar()`
     *
     * Hooked into "after_setup_theme"
     *
     * @return void
     */
    public function register()
    {
        // Define Sidebar Widget Area 1
        register_sidebar(
            [
                'name' => __('Widget Area 1', $this->textDomain),
                'description' => __('Description for this widget-area...', $this->textDomain),
                'id' => 'widget-area-1',
                'before_widget' => '<div id="%1$s" class="%2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h3>',
                'after_title' => '</h3>',
            ]
        );

        // Define Sidebar Widget Area 2
        register_sidebar(
            [
                'name' => __('Widget Area 2', $this->textDomain),
                'description' => __('Description for this widget-area...', $this->textDomain),
                'id' => 'widget-area-2',
                'before_widget' => '<div id="%1$s" class="%2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h3>',
                'after_title' => '</h3>',
            ]
        );
    }
}

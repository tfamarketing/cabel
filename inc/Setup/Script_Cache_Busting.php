<?php

namespace TFA\Setup;

use TFA\Factories\JsonQ;
use TFA\Helpers\Generic;
use TFA\Helpers\Path;
use TFA\Helpers\WP;
use TFA\Interfaces\Run;

/**
 * Adds cache busting to script/style assets
 */
class Script_Cache_Busting implements Run
{
    /**
     * Manifest json query
     *
     * @var array|Jsonq
     */
    protected $manifestJson = [];

    /**
     * JsonQ factory
     *
     * @var jsonQ
     */
    protected $jsonQ;

    public function __construct(JsonQ $jsonQ)
    {
        $this->jsonQ = $jsonQ;
    }

    public function run()
    {
        $this->getJSON();
        WP::addFilters([
            'style_loader_src',
            'script_loader_src',
        ], [$this, 'applyVersioning'], 10, 2);
    }

    /**
     * Gets manifest json query object
     *
     * @return void
     */
    public function getJSON()
    {
        if ($manifest = $this->jsonQ->getInstance(['mix-manifest.json'])) {
            $this->manifestJson = $manifest;
        }
        return $this->manifestJson;
    }

    /**
     * Applies versioning from manifest file & uses fallback method if manifest not avaliable
     *
     * Hooked into: "style_loader_src", "script_loader_src"
     *
     * Priority: 10
     *
     * @param string $src File uri of registered asset
     * @param string $handle Handle of registered asset
     * @return string
     */
    public function applyVersioning(string $src, string $handle)
    {
        $noManifestVer = true;
        if (!empty($this->manifestJson)) {
            foreach ($this->manifestJson as $manifestPath) {
                $manifestPathQuery = parse_url($manifestPath, PHP_URL_QUERY);
                $manifestPath = parse_url($manifestPath, PHP_URL_PATH);

                if (
                    !empty($manifestPathQuery) &&
                    strpos($src, Path::getThemePath($manifestPath, true)) !== false
                ) {
                    parse_str($manifestPathQuery, $manifestPathQuery);
                    if (isset($manifestPathQuery['id'])) {
                        $noManifestVer = false;
                        $src = add_query_arg('ver', $manifestPathQuery['id'], $src);
                    }
                    break;
                }
            }
        }

        if (
            !$noManifestVer &&
            strpos($src, ($themePath = Path::getThemePath('', true))) !== false
        ) {
            $relSrc = substr($src, strlen($themePath));
            $filePath = parse_url($relSrc, PHP_URL_PATH);
            $fileChangeHash = Generic::checkFileChange(Path::getThemePath($filePath), [
                'returnHash' => true,
                'handle' => $handle,
            ]);
            if ($fileChangeHash) {
                $src = add_query_arg('ver', $fileChangeHash, $src);
            }
        }

        return $src;
    }
}

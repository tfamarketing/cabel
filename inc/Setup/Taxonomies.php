<?php

namespace TFA\Setup;

use TFA\Interfaces\Run;

/**
 * Registers taxonomies
 */
class Taxonomies implements Run
{
    /**
     * Text domain
     *
     * @var string
     */
    protected $textDomain;

    public function __construct($textDomain)
    {
        $this->textDomain = $textDomain;
    }

    /**
     * @inheritDoc
     */
    public function run()
    {
        add_action('init', [$this, 'blankTag'], 0);
        add_action('init', [$this, 'blankCategory'], 0);
    }

    /**
     * Registers blank taxonomy hierarchical with `register_taxonomy()`
     *
     * Linked to post type: "tfa-blank"
     *
     * Hooked into "init"
     *
     * Priority: 0
     *
     * @return void
     */
    public function blankCategory()
    {
        $labels = [
            'name' => _x('TFA Blank Categories', 'Taxonomy General Name', $this->textDomain),
            'singular_name' => _x('TFA Blank Category', 'Taxonomy Singular Name', $this->textDomain),
            'menu_name' => __('TFA Blank Categories', $this->textDomain),
            'all_items' => __('All Categories', $this->textDomain),
            'parent_item' => __('Parent Category', $this->textDomain),
            'parent_item_colon' => __('Parent Category:', $this->textDomain),
            'new_item_name' => __('New Category Name', $this->textDomain),
            'add_new_item' => __('Add New Category', $this->textDomain),
            'edit_item' => __('Edit Category', $this->textDomain),
            'update_item' => __('Update Category', $this->textDomain),
            'view_item' => __('View Category', $this->textDomain),
            'separate_items_with_commas' => __('Separate categories with commas', $this->textDomain),
            'add_or_remove_items' => __('Add or remove categories', $this->textDomain),
            'choose_from_most_used' => __('Choose from the most used', $this->textDomain),
            'popular_items' => __('Popular Categories', $this->textDomain),
            'search_items' => __('Search Categories', $this->textDomain),
            'not_found' => __('Not Found', $this->textDomain),
            'no_terms' => __('No categories', $this->textDomain),
            'items_list' => __('Categories list', $this->textDomain),
            'items_list_navigation' => __('Categories list navigation', $this->textDomain),
        ];
        $args = [
            'labels' => $labels,
            'hierarchical' => true,
            'public' => true,
            'show_ui' => true,
            'show_admin_column' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud' => true,
        ];
        register_taxonomy('tfa-category', ['tfa-blank'], $args);
    }

    /**
     * Registers blank taxonomy non-hierarchical with `register_taxonomy()`
     *
     * Linked to post type: "tfa-blank"
     *
     * Hooked into "init"
     *
     * Priority: 0
     *
     * @return void
     */
    public function blankTag()
    {
        $labels = [
            'name' => _x('TFA Blank Tags', 'Taxonomy General Name', $this->textDomain),
            'singular_name' => _x('TFA Blank Tag', 'Taxonomy Singular Name', $this->textDomain),
            'menu_name' => __('TFA Blank Tags', $this->textDomain),
            'all_items' => __('All Tags', $this->textDomain),
            'parent_item' => __('Parent Tag', $this->textDomain),
            'parent_item_colon' => __('Parent Tag:', $this->textDomain),
            'new_item_name' => __('New Category Tag', $this->textDomain),
            'add_new_item' => __('Add New Tag', $this->textDomain),
            'edit_item' => __('Edit Tag', $this->textDomain),
            'update_item' => __('Update Tag', $this->textDomain),
            'view_item' => __('View Tag', $this->textDomain),
            'separate_items_with_commas' => __('Separate tags with commas', $this->textDomain),
            'add_or_remove_items' => __('Add or remove tags', $this->textDomain),
            'choose_from_most_used' => __('Choose from the most used', $this->textDomain),
            'popular_items' => __('Popular Tags', $this->textDomain),
            'search_items' => __('Search Tags', $this->textDomain),
            'not_found' => __('Not Found', $this->textDomain),
            'no_terms' => __('No tags', $this->textDomain),
            'items_list' => __('Tags list', $this->textDomain),
            'items_list_navigation' => __('Tags list navigation', $this->textDomain),
        ];
        $args = [
            'labels' => $labels,
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_admin_column' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud' => true,
        ];
        register_taxonomy('tfa-post-tag', ['tfa-blank'], $args);

    }
}

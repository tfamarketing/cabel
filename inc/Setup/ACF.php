<?php

namespace TFA\Setup;

use HaydenPierce\ClassFinder\ClassFinder;
use TFA\Abstracts\Field;
use TFA\Abstracts\Options_Page;
use TFA\Factories\Fields_Builder as Fields_Builder_Factory;
use TFA\Factories\Options_Page_Builder as Options_Page_Builder_Factory;
use TFA\Helpers\WP;
use TFA\Interfaces\Run;
use TFA\Storage\Fields as Fields_Storage;
use TFA\Storage\Options_Pages as Options_Pages_Storage;

/**
 * Registers field objects with ACF
 */
class ACF implements Run
{
    /**
     * Field storage container
     *
     * @var Fields_Storage
     */
    protected $fieldsStorage;

    /**
     * Fields builder factory
     *
     * @var Fields_Builder_Factory
     */
    protected $fieldsBuilderFactory;

    /**
     * Options page builder factory
     *
     * @var Options_Page_Builder_Factory
     */
    protected $optionsPageBuilderFactory;

    /**
     * Options pages storage container
     *
     * @var Options_Pages_Storage
     */
    protected $optionsPagesStorage;

    /**
     * Array of class strings belonging to the "Field" namespace
     *
     * @var array
     */
    protected $fieldClasses;

    /**
     * Array of class strings belonging to the "Options_Page" namespace
     *
     * @var array
     */
    protected $optionsPageClasses;

    public function __construct(Fields_Storage $fieldsStorage, Options_Pages_Storage $optionsPagesStorage, Options_Page_Builder_Factory $optionsPageBuilderFactory, Fields_Builder_Factory $fieldsBuilderFactory)
    {
        $this->fieldsStorage = $fieldsStorage;
        $this->fieldsBuilderFactory = $fieldsBuilderFactory;
        $this->optionsPageBuilderFactory = $optionsPageBuilderFactory;
        $this->optionsPagesStorage = $optionsPagesStorage;
    }

    /**
     * @inheritDoc
     */
    public function run()
    {
        $this->fieldClasses = ClassFinder::getClassesInNamespace('TFA\Fields', ClassFinder::RECURSIVE_MODE);
        $this->optionsPageClasses = ClassFinder::getClassesInNamespace('TFA\Options_Page', ClassFinder::RECURSIVE_MODE);
        $this->registerFieldObjects($this->fieldClasses);
        $this->registerOptionsPagesObjects($this->optionsPageClasses);
        add_action('init', [$this, 'exposeFieldsToRest']);
        add_filter('acf/load_field', [$this, 'setDefaultValue']);
    }

    /**
     * Recursively goes through `Field()` config class objects & builds/register fields
     *
     * @return array
     */
    public function registerFieldObjects(array $fields)
    {
        if ($fields ?? false) {
            foreach ($fields as &$field) {
                if (is_a($field, Field::class, true)) {
                    if (!$fieldInContainer = $this->fieldsStorage->get($field::getName())) {
                        $subFields = $field::getSubFields();
                        $subFields = $this->registerFieldObjects($subFields ?? []);
                        $fieldBuilder = $this->fieldsBuilderFactory->getInstance(
                            [
                                'name' => $field::getName(),
                                'groupConfig' => $field::getArgs(),
                            ]
                        );
                        $fieldBuilder = $field::callback($fieldBuilder, $subFields);
                        if ($field::register()) {
                            $fieldBuilder->build();
                        }
                        if ($field::shareInstance()) {
                            $this->fieldsStorage->add($fieldBuilder->getName(), $fieldBuilder);
                        }
                        $field = $fieldBuilder;
                    } else {
                        $field = $fieldInContainer;
                    }
                }
            }
            return $fields;
        }
        return [];
    }

    /**
     * Goes through `Options_Page()` config class objects & builds/register options pages
     *
     * @return array
     */
    public function registerOptionsPagesObjects(array $optionsPages)
    {
        if ($optionsPages ?? false) {
            usort($optionsPages, [$this, 'sortParentPagesFirst']);
            foreach ($optionsPages as &$optionsPage) {
                if (is_a($optionsPage, Options_Page::class, true)) {
                    if (!$optionsPageInContainer = $this->optionsPagesStorage->get($optionsPage::getName())) {
                        $optionsPageBuilder = $this->optionsPageBuilderFactory->getInstance(
                            [
                                'name' => $optionsPage::getName(),
                            ]
                        );
                        $optionsPageBuilder = $optionsPage::callback($optionsPageBuilder);
                        if ($parentPage = $optionsPage::getParentPage()) {
                            $optionsPageBuilder
                                ->setParentSlug($parentPage::getName());
                        }
                        $this->optionsPagesStorage->add($optionsPage::getName(), $optionsPageBuilder);
                        $optionsPageBuilder->build();
                        $optionsPage = $optionsPageBuilder;
                    } else {
                        $optionsPage = $optionsPageInContainer;
                    }
                }
            }
            return $optionsPages;
        }
        return [];
    }

    /**
     * Custom sorting function for `uasort()` to put root pages first
     *
     * @param Options_Page|string $a
     * @param Options_Page|string $b
     * @return int
     */
    public function sortParentPagesFirst($a, $b)
    {
        if (!(is_a($a, Options_Page::class, true) && is_a($b, Options_Page::class, true))) {
            return -1;
        }
        return empty($b::getParentPage()) <=> empty($a::getParentPage());
    }

    /**
     * Exposes fields to rest API for all post types
     *
     * Hooked into: "init"
     *
     * @return void
     */
    public function exposeFieldsToRest()
    {
        $postTypes = array_map(function ($postType) {
            return "rest_prepare_{$postType}";
        }, (get_post_types() ?: []));

        if (!empty($postTypes)) {
            WP::addFilters($postTypes, function ($response, $post) {
                if (isset($post)) {
                    $acf = get_fields($post->id);
                    $response->data['acf'] = $acf;
                }
                return $response;
            }, 10, 2);
        }
    }

    /**
     * Sets default value on field if no value exists in the database
     *
     * @param array $field Field config array
     * @return array
     */
    public function setDefaultValue(array $field)
    {
        global $post;
        if (
            !empty($post) &&
            !empty($field['value']) &&
            !empty($field['name']) &&
            (get_post_meta(($post->ID ?? 0), $field['name'], true) ?: false)
        ) {
            $field['value'] = null;
        }
        return $field;
    }
}

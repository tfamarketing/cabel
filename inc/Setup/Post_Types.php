<?php

namespace TFA\Setup;

use TFA\Interfaces\Run;

/**
 * Registers post types
 */
class Post_Types implements Run
{
    /**
     * Text domain
     *
     * @var string
     */
    protected $textDomain;

    public function __construct($textDomain)
    {
        $this->textDomain = $textDomain;
    }

    /**
     * @inheritDoc
     */
    public function run()
    {
        add_action('init', [$this, 'blank'], 0);
    }

    /**
     * Registers blank post type with `register_post_type()`
     *
     * Hooked into "init"
     *
     * Priority: 0
     *
     * @return void
     */
    public function blank()
    {
        $labels = [
            'name' => _x('TFA Blank Custom Posts', 'Post Type General Name', $this->textDomain),
            'singular_name' => _x('TFA Blank Custom Post', 'Post Type Singular Name', $this->textDomain),
            'menu_name' => __('TFA Blank', $this->textDomain),
            'name_admin_bar' => __('TFA Blank', $this->textDomain),
            'archives' => __('Item Archives', $this->textDomain),
            'attributes' => __('Item Attributes', $this->textDomain),
            'parent_item_colon' => __('Parent Item:', $this->textDomain),
            'all_items' => __('All Items', $this->textDomain),
            'add_new_item' => __('Add New Item', $this->textDomain),
            'add_new' => __('Add New', $this->textDomain),
            'new_item' => __('New Item', $this->textDomain),
            'edit_item' => __('Edit Item', $this->textDomain),
            'update_item' => __('Update Item', $this->textDomain),
            'view_item' => __('View Item', $this->textDomain),
            'view_items' => __('View Items', $this->textDomain),
            'search_items' => __('Search Item', $this->textDomain),
            'not_found' => __('Not found', $this->textDomain),
            'not_found_in_trash' => __('Not found in Trash', $this->textDomain),
            'featured_image' => __('Featured Image', $this->textDomain),
            'set_featured_image' => __('Set featured image', $this->textDomain),
            'remove_featured_image' => __('Remove featured image', $this->textDomain),
            'use_featured_image' => __('Use as featured image', $this->textDomain),
            'insert_into_item' => __('Insert into item', $this->textDomain),
            'uploaded_to_this_item' => __('Uploaded to this item', $this->textDomain),
            'items_list' => __('Items list', $this->textDomain),
            'items_list_navigation' => __('Items list navigation', $this->textDomain),
            'filter_items_list' => __('Filter items list', $this->textDomain),
        ];

        $args = [
            'label' => __('TFA Blank Custom Post', $this->textDomain),
            'description' => __('A blank example custom post type', $this->textDomain),
            'labels' => $labels,
            'supports' => array('title', 'editor', 'thumbnail', 'revisions'),
            'taxonomies' => array('tfa-blank-category', 'tfa-blank-post_tag'),
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 5,
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type' => 'page',
        ];
        register_post_type('tfa-blank', $args);
    }
}

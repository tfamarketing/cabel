<?php

namespace TFA\Setup;

use CaseConverter\CaseString;
use TFA\Helpers\Generic;
use TFA\Interfaces\Run;

/**
 * Setup class for TinyMce editor
 */
class TinyMce implements Run
{
    public function run()
    {
        add_filter('tiny_mce_before_init', [$this, 'addCustomColours']);
    }

    /**
     * Replaces colours for TinyMce colour swatch
     *
     * Hooked into: "tiny_mce_before_init"
     *
     * @param array $init
     * @return array
     */
    public function addCustomColours($init)
    {
        $colours = Generic::getJsonArray(Generic::getFile('src/styles/variables.json'));
        $colours = $this->coloursArrayToString($colours);
        $init['textcolor_map'] = "[$colours]";
        return $init;
    }

    /**
     * Converts colours array into TinyMce friendly string
     *
     * @param array $colours
     * @return string
     */
    protected function coloursArrayToString(array $colours)
    {
        $colourMap = '';
        if (!empty($colours)) {
            $counter = 1;
            $totalCount = count($colours);
            foreach ($colours as $name => $colour) {
                if ((!empty($colour)) && (!empty($name))) {
                    if (strpos($colour, '#') !== false) {
                        $colour = substr($colour, 1);
                    }

                    $name = CaseString::kebab($name)->snake();
                    $name = CaseString::snake($name)->title();

                    $colourMap .= "\"$colour\", \"$name\"";
                    $colourMap .= $counter != $totalCount ? ', ' : '';

                    $counter++;
                }
            }
        }
        return $colourMap;
    }
}

<?php

namespace TFA\Factories;

use Nahid\JsonQ\Jsonq as JsonQ_Lib;
use TFA\Abstracts\Factory;
use TFA\Helpers\Path;

/**
 * Factory class for `Nahid\JsonQ\Jsonq()`
 */
class JsonQ extends Factory
{
    public $className = JsonQ_Lib::class;

    /**
     * @inheritDoc
     */
    protected function getArgs(array $args)
    {
        if (!empty($args[0]) || !empty($args['data'])) {
            if (!empty($args[0]) && empty($args['data'])) {
                $args[0] = $this->getTemplatePath($args[0]);
            } elseif (!empty($args['data']) && empty($args[0])) {
                $args['data'] = $this->getTemplatePath($args['data']);
            }
        }
        return $args;
    }

    /**
     * Gets full template path
     *
     * @param string $path
     * @return string
     */
    protected function getTemplatePath(string $path)
    {
        return Path::getThemePath($path);
    }
}

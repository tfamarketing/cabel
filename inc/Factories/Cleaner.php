<?php

namespace TFA\Factories;

use TFA\Abstracts\Static_Factory;
use TFA\Libs\Cleaner as Cleaner_Libs;

/**
 * Static factory for `TFA\Libs\Cleaner()`
 */
class Cleaner extends Static_Factory
{
    public static $className = Cleaner_Libs::class;
}

<?php

namespace TFA\Factories;

use TFA\Abstracts\Static_Factory;
use WP_Bootstrap_Navwalker as WP_Bootstrap_Navwalker_Parent;

/**
 * Factory class for `TFA\Libs\Fields_Builder\Fields_Builder()`
 */
class WP_Bootstrap_Navwalker extends Static_Factory
{
    public static $className = WP_Bootstrap_Navwalker_Parent::class;
}

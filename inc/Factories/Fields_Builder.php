<?php

namespace TFA\Factories;

use TFA\Abstracts\Factory;
use TFA\Libs\Fields_Builder as Fields_Builder_Lib;

/**
 * Factory class for `TFA\Libs\Fields_Builder\Fields_Builder()`
 */
class Fields_Builder extends Factory
{
    public $className = Fields_Builder_Lib::class;
}

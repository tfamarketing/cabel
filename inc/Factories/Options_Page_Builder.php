<?php

namespace TFA\Factories;

use TFA\Abstracts\Factory;
use TFA\Libs\Options_Page_Builder as Options_Page_Builder_Lib;

/**
 * Factory class for `TFA\Libs\Options_Page_Builder()`
 */
class Options_Page_Builder extends Factory
{
    public $className = Options_Page_Builder_Lib::class;
}

<?php

namespace TFA\Interfaces;

/**
 * Interface for static factory
 */
interface Static_Factory
{
    /**
     * Creates and then returns object instance
     *
     * @return object|null
     */
    public static function getInstance();
}

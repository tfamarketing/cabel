<?php

namespace TFA\Interfaces;

/**
 * Interface for factory
 */
interface Factory
{
    /**
     * Creates and then returns object instance
     *
     * @return object|null
     */
    public function getInstance();
}

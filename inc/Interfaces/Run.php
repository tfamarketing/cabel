<?php

namespace TFA\Interfaces;

/**
 * Bootstrap interface
 */
interface Run
{
    /**
     * Main initalize method for class
     *
     * @return void
     */
    public function run();
}

<?php

namespace TFA\Interfaces;

/**
 * Interface for classes that wish to share their instances
 */
interface Share
{
    /**
     * Determines whether instance should be shared in the DI container
     *
     * @return boolean
     */
    public function shareInstance();
}

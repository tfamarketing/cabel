<?php

namespace TFA\Interfaces;

use TFA\Libs\Options_Page_Builder;

/**
 * Interface for options page classes
 */
interface Options_Page
{
    /**
     * Set options page name
     *
     * @return string
     */
    public static function getName();

    /**
     * Return `Fields_Builder` instance after adding arguments
     *
     * @param Options_Page_Builder $optionsPage
     * @return Options_Page_Builder
     */
    public static function callback(Options_Page_Builder $optionsPage);

    /**
     * Returns class string for parent page
     *
     * @return string
     */
    public static function getParentPage();
}

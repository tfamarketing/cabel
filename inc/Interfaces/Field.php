<?php

namespace TFA\Interfaces;

use TFA\Libs\Fields_Builder;

/**
 * Interface for field classes
 */
interface Field
{
    /**
     * Set field name
     *
     * @return string
     */
    public static function getName();

    /**
     * Set additional arguments for field
     *
     * @return array
     */
    public static function getArgs();

    /**
     * Set sub-fields that will be associated this field
     *
     * Return array of namespaced field classes
     *
     * @return array
     */
    public static function getSubFields();

    /**
     * Return boolean to enable field registration with ACF
     *
     * @return boolean
     */
    public static function register();

    /**
     * Return boolean to share instance into container
     *
     * This is a hotfix for issues regarding modifying fields & the modified fields being shared instead of the original config
     *
     * @return boolean
     */
    public static function shareInstance();

    /**
     * Return `Fields_Builder` instance after adding arguments
     *
     * @param Fields_Builder $field
     * @return Fields_Builder
     */
    public static function callback(Fields_Builder $field, array $subFields);
}

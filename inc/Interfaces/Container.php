<?php

namespace TFA\Interfaces;

/**
 * Interface for container
 */
interface Container
{
    /**
     * Adds to container
     *
     * @param mixed $id - Key in container
     * @param mixed $value - Value saved
     * @return void
     */
    public function add($id, $value);

    /**
     * Retrieve key value from container
     *
     * @param mixed $id
     * @return mixed
     */
    public function get($id);

    /**
     * Removes key from container
     *
     * @param mixed $id
     * @return void
     */
    public function remove($id);
}

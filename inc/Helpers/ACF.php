<?php

namespace TFA\Helpers;

/**
 * Helper class for ACF related functions
 */
class ACF
{
    /**
     * Gets all flexible content fields from layout name, designates them to a template & returns result
     *
     * @param string $flexGroupName Name of flexible content field group
     * @param array $additionalTemplates Add additional templates that do not start with prefix
     * @param mixed $post Grab layouts from specfifc post object/ID
     * @return string|bool
     */
    public static function getFlexibleLayouts(string $flexGroupName, array $additionalTemplates = [], $post = false)
    {
        $return = '';
        $post = !empty($post) ? Post::getPost($post) : false;
        if (have_rows($flexGroupName, $post)) {
            $files = Generic::createArrayFromFiles('templates');
            $files = array_merge($files, $additionalTemplates);
            $return .= "<div class=\"flexible-content $flexGroupName\">";
            while (have_rows($flexGroupName, $post)) {
                $theRow = the_row(true);
                $layout = $theRow['acf_fc_layout'] ?? '';
                $layout = str_replace('_', '-', $layout);
                $theRow = Generic::changeCaseArrayKeys($theRow, 'snake', 'camel');
                if (array_key_exists($filename = "flexible-{$layout}", $files) || array_key_exists($filename = $layout, $files)) {
                    $return .= Template::getPart($files[$filename], $theRow);
                }
            }
            $return .= "</div>";
        }
        return empty($return) ? false : $return;
    }

    /**
     * Checks filename to determine if it's prefixed with "flexible-"
     *
     * @param string $filename
     * @return boolean
     */
    public static function checkIfFlexibleParital(string $filename)
    {
        $filename = pathinfo($filename, PATHINFO_FILENAME);
        if (substr($filename, 0, strlen($filename)) === 'flexible-') {
            return true;
        }
        return false;
    }

    /**
     * Gets ACF option field by language code, using default language by default
     *
     * @param string $optionName Name of option
     * @param string $languageCode Language code
     * @return mixed
     */
    public static function getLanguageOption(string $optionName = '', string $languageCode = '')
    {
        if (!empty($optionName)) {
            $setDefaultLang = function () use ($languageCode) {
                return $languageCode ?: acf_get_setting('default_language');
            };

            add_filter('acf/settings/current_language', $setDefaultLang, 100);
            $option = (empty($optionName) ? get_fields('option') : get_field($optionName, 'option'));
            remove_filter('acf/settings/current_language', $setDefaultLang, 100);

            return $option;
        }
        return false;
    }
}

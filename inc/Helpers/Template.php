<?php

namespace TFA\Helpers;

/**
 * Helper class for template related functions
 */
class Template
{
    /**
     * Include template parts with injected arguments
     *
     * @param string $file - The name of the file you wish to include
     * @param array $templateArgs - The array of arguments you wish to inject
     * @return string|bool
     */
    public static function getPart(string $file, array $templateArgs = [])
    {
        $fileparts = pathinfo($file);
        $extensions = ['php', 'html'];

        $fileHandle = $fileparts['filename'];

        if (array_key_exists("extension", $fileparts)) {
            if (!in_array($fileparts['extension'], $extensions)) {
                throw new \InvalidArgumentException('Invalid file type');
            }
        } else {
            $file = "$file.php";
        }

        do_action('start_operation', 'tfa_template_part::' . $fileHandle);

        $file = file_exists($file) ? $file : Path::getThemePath($file);

        if ($file ?? false) {
            global $posts, $post, $wp_did_header, $wp_query, $wp_rewrite, $wpdb, $wp_version, $wp, $id, $comment, $user_ID;
            $templateArgs = wp_parse_args($templateArgs);

            ob_start();
            extract($templateArgs);
            unset($templateArgs);
            $returnRequire = include $file;
            $data = ob_get_clean();

            do_action('end_operation', 'tfa_template_part::' . $fileHandle);

            if ($returnRequire) {
                return $data;
            }
        }

        return false;
    }
}

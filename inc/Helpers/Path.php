<?php

namespace TFA\Helpers;

/**
 * Helper class for theme path related functions
 */
class Path
{
    /**
     * Gets theme path or URI
     *
     * @param string $subPath - Dir or file path
     * @param boolean $uri - Returns uri if true
     * @return string|bool
     */
    public static function getThemePath(string $subPath = '', bool $uri = false)
    {
        $subPath = ltrim($subPath, '/\\');
        $subPath = rtrim($subPath, '/\\');

        if ($uri) {
            $themePath = get_theme_file_uri($subPath);
        } else {
            $themePath = locate_template((empty($subPath) ? '/' : $subPath), false, false);
            $themePath = rtrim($themePath, '/');
            $themePath = realpath($themePath);
        }

        return empty($themePath) ? false : $themePath;
    }

    /**
     * Takes full path/uri and makes relative to theme root
     *
     * @param string $path Full path or uri
     * @param bool $removingPrecedingSlash Removes proceding slash from result
     * @return string
     */
    public static function getRelativePath(string $path, bool $removingPrecedingSlash = true)
    {
        if (!empty($path)) {
            if ((filter_var($path, FILTER_VALIDATE_URL) &&
                    strpos($path, $themePath = get_stylesheet_directory_uri()) !== false ||
                    strpos($path, $themePath = get_template_directory_uri()) !== false) ||
                (file_exists($path = realpath($path)) &&
                    strpos($path, $themePath = realpath(get_stylesheet_directory())) !== false ||
                    strpos($path, $themePath = realpath(get_template_directory())) !== false)
            ) {
                $path = substr($path, strlen($themePath));
                return $removingPrecedingSlash ? ltrim($path, '/\\') : $path;
            }
        }
        return $path;
    }

    /**
     * Gets current url
     *
     * @return string
     */
    public static function getCurrentUrl()
    {
        global $wp;
        return home_url(($wp->request ?? '') . '/');
    }

    /**
     * Checks if URL is current based on request
     *
     * @param string $url Url to check
     * @return bool
     */
    public static function checkUrlIsCurrent(string $url)
    {
        if (!empty($url)) {
            $requestUrl = static::getCurrentUrl();
            return $url == $requestUrl;
        }
        return false;
    }
}

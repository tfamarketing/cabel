<?php

namespace TFA\Helpers;

/**
 * Helper class for page related functions
 */
class Page
{
    /**
     * Gets root parent ancestor from post
     *
     * @param int|WP_Post|null $post
     * @return WP_Post|null
     */
    public static function getRootParent($post)
    {
        $post = Post::getPost($post);
        $ancestors = get_ancestors($post->ID, $post->post_type, 'post_type');
        $rootPage = (!empty($ancestors)) ? end($ancestors) : $post;
        return Post::getPost($rootPage);
    }

    /**
     * Recursive function to get children of WP_Post
     *
     * @param int|WP_Post|null $post - WP Post object or id
     * @param int|null $depth - The recursion depth
     * @param boolean $returnFlatArray - Return a flat array of post objects
     * @param array $excludePosts - An array of WP Post objects or ids that will be excluded from the return
     * @param array $queryArgs - WP_Query arguments
     * @return void
     */
    public static function getChildren($post, $depth = null, bool $returnFlatArray = false, array $excludePosts = [], array $queryArgs = [])
    {
        $post = Post::getPost($post);
        static $recursionDepth = 0;

        if ($post ?? false) {
            if ($returnFlatArray) {
                $flatArray = [];
            }

            (array) $children = get_children(array_merge(
                [
                    'post_status' => 'publish',
                    'order' => 'ASC',
                    'orderby' => 'post_title',
                ],
                $queryArgs,
                [
                    'post_type' => $post->post_type,
                    'post_parent' => $post->ID,
                ]
            ));

            foreach (($excludePosts ?? []) as $excludedPost) {
                $excludedPost = Post::getPost($excludedPost);
                if (($excludedPost->ID ?? false) && array_key_exists($excludedPost->ID, $children)) {
                    unset($children[$excludedPost->ID]);
                }
            }

            foreach ($children as &$child) {
                $recursionDepth++;

                $gChildren = static::getChildren($child, $depth, $returnFlatArray, $excludePosts, $queryArgs);
                if (($gChildren ?? false) && ((!is_null($depth)) && is_numeric($depth) ? ($depth >= $recursionDepth) : true)) {
                    if (!$returnFlatArray) {
                        $child->children = $gChildren;
                    } else {
                        $flatArray = $children + $gChildren;
                    }
                }

                if (!$returnFlatArray) {
                    $child->depth = $recursionDepth;
                }

                $recursionDepth--;
            }

            return $returnFlatArray ? $flatArray + $children : $children;
        }

        return [];
    }

    /**
     * Returns a breadcrumb string of the all the ancestors of the post
     *
     * @param int|WP_Post|null $post WP_Post object or id
     * @param string $seperator The seperator between links
     * @param bool $includePost Includes post variable
     * @param bool $includeHomePage Includes front/home page,
     * @param array $additionalItems Include arbituary items
     * @return string
     */
    public static function ancestorBreadcrumb($post, string $seperator = '>', bool $includePost = false, bool $includeHomePage = false, array $additionalItems = [])
    {
        global $wp;
        $post = Post::getPost($post);
        $pageParentString = '';
        $ancestors = get_post_ancestors($post);

        if ($includeHomePage) {
            $pageOnFront = (int) get_option('page_on_front');
            $includeHomePage = [$pageOnFront];
        } else {
            $includeHomePage = [];
        }

        if ($includePost && isset($pageOnFront) && $pageOnFront != $post->ID) {
            $includePost = [$post->ID];
        } else {
            $includePost = [];
        }

        if (($postTypeObject = get_post_type_object($post->post_type))
            && ($postTypeObject->has_archive ?? false)) {
            if ($postArchive = get_page_by_path($postTypeObject->rewrite['slug'])) {
                $postArchive = [$postArchive];
            } else {
                $postArchive[] = [
                    'pageParentTitle' => $postTypeObject->label,
                    'pageParentLink' => get_post_type_archive_link($postTypeObject->name),
                ];
            }
        } else {
            $postArchive = [];
        }

        $ancestors = array_merge($additionalItems, $includePost, $ancestors, $postArchive, $includeHomePage);

        $ancestors = array_reverse($ancestors);

        if (!empty($ancestors)) {
            $i = 0;
            $ancestorsCount = count($ancestors);
            $pageParentString .= '<div class="ancestor-breadcrumb">';
            foreach ($ancestors as $ancestor) {
                $i++;
                if (is_numeric($ancestor) || $ancestor instanceof \WP_Post) {
                    $pageParentTitle = get_the_title($ancestor);
                    $pageParentLink = get_permalink($ancestor);
                } elseif (is_array($ancestor) &&
                    array_key_exists('pageParentLink', $ancestor) &&
                    array_key_exists('pageParentTitle', $ancestor)
                    && count($ancestor) == 2) {
                    extract($ancestor);
                } else {
                    $pageParentTitle = 'Homepage';
                    $pageParentLink = home_url('/');
                }

                $isActive = Path::checkURLIsCurrent($pageParentLink) ? ' page-parent-link-active' : '';

                $pageParentString .= "<a href=\"$pageParentLink\" class=\"page-parent-link$isActive\">$pageParentTitle</a>";
                if ((!empty($seperator)) && $ancestorsCount > $i && $ancestorsCount != $i) {
                    $pageParentString .= "<span class=\"separator\">$seperator</span>";
                }
            }
            $pageParentString .= '</div>';
        }

        return $pageParentString;
    }

    /**
     * Gets permalink of post relative to home URL
     *
     * @param WP_Post|int|null $post
     * @return bool|string
     */
    public static function getRelativePermalink($post)
    {
        if (!empty($post) &&
            (is_numeric($post) || $post instanceof \WP_Post) &&
            !empty($permalink = get_permalink($post))) {
            return rtrim(substr($permalink, strlen(home_url('/'))), '/');
        }
        return false;
    }
}

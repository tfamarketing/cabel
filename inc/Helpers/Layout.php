<?php

namespace TFA\Helpers;

/**
 * Helper class for layout related functions
 */
class Layout
{
    /**
     * Based on integer, will return bootstrap columns
     *
     * @param int $amount Amount that will produce columns result
     * @param string $breakpoint Bootstrap breakpoint to be used in column class
     * @param int $maxAmount Max amount of colums
     * @param bool $returnInt Return integer instead of class string
     * @return string|int|bool
     */
    public static function getBootstrapColumns(int $amount, string $breakpoint = 'md', int $maxAmount = 4, bool $returnInt = false)
    {
        if ($amount > 0) {
            $condition = function (int $int) use ($amount, $maxAmount) {
                return ($maxAmount > 0 && $int >= $maxAmount) || ($amount == $int);
            };

            if ($amount >= 12) {
                $col = 1;
            } elseif ($condition(1)) {
                $col = 12;
            } elseif ($condition(2)) {
                $col = 6;
            } elseif ($condition(3)) {
                $col = 4;
            } elseif ($condition(4)) {
                $col = 3;
            } elseif ($condition(5)) {
                $col = 2;
            }

            $breakpoint = !empty($breakpoint) ? "-{$breakpoint}" : '';
            return $returnInt ? $col : "col{$breakpoint}-{$col}";
        }

        return false;
    }
}

<?php

namespace TFA\Helpers;

/**
 * Helper class for pagination functions
 */
class Pagination
{
    /**
     * Global wrapper for `paginate_links()`
     *
     * @return string
     */
    public static function global()
    {
        global $wp_query;
        $big = 999999999;
        return paginate_links(array(
            'base' => str_replace($big, '%#%', get_pagenum_link($big)),
            'format' => '?paged=%#%',
            'current' => max(1, get_query_var('paged')),
            'total' => $wp_query->max_num_pages,
        ));
    }

    /**
     * Wraps `get_previous_post_link()` & `get_next_post_link()` for basic pagination on single posts
     *
     * @param string $prevLink Previous link text
     * @param string $nextLink Next link text
     * @return string
     */
    public static function single(string $prevLink = '', string $nextLink = '')
    {
        $html = '';
        $format = '%link';
        $previous = get_previous_post_link($format, (!empty($prevLink) ? $prevLink : 'Previous'));
        $next = get_next_post_link($format, (!empty($nextLink) ? $nextLink : 'Next'));
        if (!empty($previous) || !empty($next)) {
            $html .= '<div class="single-pagination">';
            if (!empty($previous)) {
                $html .= '<div class="prev">' . $previous . '</div>';
            }
            if (!empty($next)) {
                $html .= '<div class="next">' . $next . '</div>';
            }
            $html .= '</div>';
        }
        return $html;
    }
}

<?php

namespace TFA\Helpers;

use TFA\Factories\WP_Bootstrap_Navwalker;

/**
 * Helper class for navigation related functions
 */
class Navigation
{
    /**
     * Wraps wordpress `wp_nav_menu()` for default config of footer menu
     *
     * @param string $location Nav menu location key
     * @param array $args Additional arguments
     * @return string|bool
     */
    public static function getMenu(string $location, array $args = [])
    {
        if (has_nav_menu($location)) {
            return wp_nav_menu(
                array_merge(
                    [
                        'menu' => '',
                        'container' => 'div',
                        'container_class' => 'menu-{menu slug}-container',
                        'container_id' => '',
                        'menu_class' => "menu $location",
                        'menu_id' => "menu-$location",
                        'fallback_cb' => '\WP_Bootstrap_Navwalker::fallback',
                        'before' => '',
                        'after' => '',
                        'link_before' => '',
                        'link_after' => '',
                        'depth' => 0,
                        'walker' => WP_Bootstrap_Navwalker::getInstance(),
                    ],
                    $args,
                    [
                        'theme_location' => $location,
                        'echo' => false,
                    ]
                )
            );
        }
        return false;
    }
}

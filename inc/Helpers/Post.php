<?php

namespace TFA\Helpers;

/**
 * Helper class for post related functions
 */
class Post
{
    /**
     * Wraps `the_excerpt()` with buffer so it can be returned
     *
     * @return string|bool
     */
    public static function getTheExcerpt()
    {
        ob_start();
        the_excerpt();
        return (ob_get_clean() ?: false);
    }

    /**
     * Returns string of all taxonomy terms of post
     *
     * @param int|WP_Post|null $post - WP_Post object or id
     * @return string
     */
    public function taxonomyTerms($post)
    {
        $termsString = '';
        $post = static::getPost($post);
        $postTaxonomies = get_object_taxonomies($post->post_type, 'object');
        $i = 0;
        foreach ($postTaxonomies as $postTaxonomy) {
            $terms = get_the_terms($post, $postTaxonomy->name);
            $taxonomyTerms = [];

            foreach ($terms as $term) {
                if ($term->taxonomy == $postTaxonomy->name) {
                    $taxonomyTerms[$term->taxonomy][] = $term->name;
                }
            }

            $taxonomyTermsCount = count($taxonomyTerms);

            if (tfa_check_array_key_value($postTaxonomy->name, $taxonomyTerms)) {
                $explodedTerms = implode(', ', $taxonomyTerms[$postTaxonomy->name]);
                $termsString .= "{$postTaxonomy->label} - $explodedTerms";
                if ($taxonomyTermsCount >= 1 && $i != $taxonomyTermsCount) {
                    $termsString .= '<br>';
                }
            }

            $i++;
        }

        return $termsString;
    }

    /**
     * Wraps function with excerpt filter
     *
     * @param string $length_callback
     * @param string $more_callback
     * @return string
     */
    public static function excerpt(string $length_callback = '', string $more_callback = '')
    {
        $blank_callback = function () {
            return '';
        };

        if (function_exists($length_callback)) {
            add_filter('excerpt_length', $length_callback);
        }

        if (function_exists($more_callback)) {
            add_filter('excerpt_more', $blank_callback);
        }

        ob_start();
        the_excerpt();
        $output = ob_get_clean();

        if (function_exists($length_callback)) {
            remove_filter('excerpt_length', $length_callback);
        }

        if (function_exists($more_callback) && ($output ?? false)) {
            $output = call_user_func($more_callback, $output);
            remove_filter('excerpt_more', $blank_callback);
        }

        return $output;
    }

    /**
     * Breadcrumb containg homepage (if included), archive & post
     *
     * @param mixed $post
     * @param string $seperator
     * @param bool $includeHomePage
     * @param array $additionalItems
     * @return string
     */
    public static function breadcrumb($post, string $seperator = '>', bool $includeHomePage = true, array $additionalItems = [])
    {
        $breadCrumb = '';
        if (!empty($post = static::getPost($post))) {
            $breadCrumbList = [];
            $postTypeObject = get_post_type_object($post->post_type);

            if ($additionalItems) {
                $breadCrumbList = array_merge($breadCrumbList, $additionalItems);
            }

            if ($page = static::getArchivePage($postTypeObject)) {
                $ancestors = get_post_ancestors($page);
                $ancestors = array_merge($ancestors, [$page]);
                $ancestors = array_map(function ($page) {
                    return [
                        'title' => get_the_title($page),
                        'link' => get_permalink($page),
                    ];
                }, $ancestors);

                $breadCrumbList = array_merge($breadCrumbList, $ancestors);
            } else {
                $breadCrumbList[] = [
                    'title' => $postTypeObject->labels->name,
                    'link' => get_post_type_archive_link($post->post_type)
                ];
            }

            $breadCrumbList[] = [
                'title' => get_the_title($post),
                'link' => get_permalink($post)
            ];

            if ($includeHomePage) {
                $pageOnFront = (int) get_option('page_on_front');

                $breadCrumbList = array_merge([
                    [
                        'title' => !empty($pageOnFront) ? get_the_title($pageOnFront) : 'Homepage',
                        'link' => !empty($pageOnFront) ? get_permalink($pageOnFront) : home_url('/'),
                    ],
                ], $breadCrumbList);
            }

            $i = 0;
            $breadCrumbCount = count($breadCrumbList);
            $breadCrumb .= '<div class="post-breadcrumb">';
            foreach ($breadCrumbList as $breadCrumbSegment) {
                $currentPost = (get_permalink() == $breadCrumbSegment['link'] ? ' post-link-active' : '');
                $i++;
                $breadCrumb .= "<a href=\"{$breadCrumbSegment['link']}\" class=\"post-link$currentPost\">{$breadCrumbSegment['title']}</a>";
                if ((!empty($seperator)) && $breadCrumbCount > $i && $breadCrumbCount != $i) {
                    $breadCrumb .= "<span class=\"separator\">$seperator</span>";
                }
            }
            $breadCrumb .= '</div>';
        }
        return $breadCrumb;
    }

    /**
     * Wrapper around `get_post()`
     *
     * Gets post in admin area when post string is available
     *
     * @param int|WP_Post|null $post
     * @param string $output
     * @param string $filter
     * @return WP_Post|null
     */
    public static function getPost($post = null, string $output = OBJECT, string $filter = 'raw')
    {
        if (is_admin() && empty($post)) {
            $post = (int) filter_input(INPUT_GET, 'post');
        }

        return get_post($post ?? null, $output, $filter);
    }

    /**
     * Copy of `get_post_type()` but works in the admin area
     *
     * @param int|WP_Post|null $post
     * @return string|bool
     */
    public static function getPostType($post = null)
    {
        $post = static::getPost($post);
        if (is_admin() && empty($post)) {
            $postType = (string) filter_input(INPUT_GET, 'post_type');
        }
        if (
            !empty($post = ($post->post_type ?? null)) ||
            (isset($postType) && !empty($post = $postType))
        ) {
            return $post;
        }
        return false;
    }

    /**
     * Gets page that is set to post type archive
     *
     * @param string|WP_Post_Type $postType Post type that archive is associated with
     * @return bool|WP_Post
     */
    public static function getArchivePage($postType)
    {
        if (!empty($postType)) {
            if (is_string($postType)) {
                $postType = get_post_type_object($postType);
            }

            $postTypeName = ($postType->name ?? '');

            if (
                $postType instanceof \WP_Post_Type &&
                (($hasArchive = $postType->has_archive ?? false) ||
                    $postTypeName == 'post')
            ) {
                if ($postTypeName == 'post') {
                    $archivePage = get_post((int) get_option('page_for_posts')) ?: null;
                } elseif (
                    $postTypeName == 'product' &&
                    (function_exists('wc_get_page_id') && wc_get_page_id('shop'))
                ) {
                    $archivePage = get_post((int) wc_get_page_id('shop'));
                } elseif ($hasArchive) {
                    $archivePage = $postType->archive_page ?? null;
                }
                return $archivePage ?? false;
            }
        }
        return false;
    }

    public static function getPagedTerms(string $taxonomy, array $args = [])
    {
        if (!empty($taxonomy) && taxonomy_exists($taxonomy)) {
            $termsPerPage = get_option('posts_per_page', 12);
            $paged = 1;

            if (array_key_exists('paged', $args)) {
                $paged = (int) $args['paged'];
                unset($args['paged']);
            }

            if (array_key_exists('terms_per_page', $args)) {
                $termsPerPage = (int) $args['terms_per_page'];
                unset($args['terms_per_page']);
            }

            $termCount = get_terms($taxonomy, array_merge($args, ['fields' => 'count']));

            if (!empty($termCount)) {
                $termCount = (int) $termCount;

                if (get_query_var('paged')) {
                    $basePaged = (int) get_query_var('paged');
                } elseif (get_query_var('page')) {
                    $basePaged = (int) get_query_var('page');
                } else {
                    $basePaged = 1;
                }

                $paged = ($paged <= $basePaged ? $paged : ($basePaged + $paged));
                $offset = (($paged - 1) * $termsPerPage);

                $args = array_merge($args, [
                    'number' => $termsPerPage,
                    'offset' => $offset
                ]);

                $terms = get_terms($taxonomy, $args);

                return $terms;
            }
        }
        return false;
    }
}

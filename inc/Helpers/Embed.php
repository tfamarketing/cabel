<?php

namespace TFA\Helpers;

/**
 * Helper class for embed related functions
 */
class Embed
{
    /**
     * Gets thumbnail from video based on provider
     *
     * @see https://support.advancedcustomfields.com/forums/topic/youtube-thumbnail-object-with-oembed-field/
     * @param string $videoUrl Url of video
     * @return string
     */
    public static function getVideoThumbnailUri(string $videoUrl)
    {
        $thumbnail_uri = '';

        // determine the type of video and the video id
        $video = static::parseVideoUri($videoUrl);

        // get youtube thumbnail
        if ($video['type'] == 'youtube')
            $thumbnail_uri = 'http://img.youtube.com/vi/' . $video['id'] . '/hqdefault.jpg';

        // get vimeo thumbnail
        if ($video['type'] == 'vimeo')
            $thumbnail_uri = static::getVimeoThumbnailUri($video['id']);
        // get wistia thumbnail
        if ($video['type'] == 'wistia')
            $thumbnail_uri = static::getWistiaThumbnailUri($videoUrl);
        // get default/placeholder thumbnail
        if (empty($thumbnail_uri) || is_wp_error($thumbnail_uri))
            $thumbnail_uri = '';

        //return thumbnail uri
        return $thumbnail_uri;
    }


    /**
     * Parse the video uri/url to determine the video type/source and the video id
     *
     * @see https://support.advancedcustomfields.com/forums/topic/youtube-thumbnail-object-with-oembed-field/
     * @param string $url Url of video
     * @return bool|array
     */
    public static function parseVideoUri(string $url)
    {
        // Parse the url
        $parse = parse_url($url);

        // Set blank variables
        $videoType = '';
        $videoId = '';

        // Url is http://youtu.be/xxxx
        if ($parse['host'] == 'youtu.be') {
            $videoType = 'youtube';
            $videoId = ltrim($parse['path'], '/');
        }

        // Url is http://www.youtube.com/watch?v=xxxx
        // or http://www.youtube.com/watch?feature=player_embedded&v=xxx
        // or http://www.youtube.com/embed/xxxx
        if (($parse['host'] == 'youtube.com') || ($parse['host'] == 'www.youtube.com')) {
            $videoType = 'youtube';
            parse_str($parse['query'], $v);
            $videoId = $v;

            if (!empty($feature)) {
                $result = explode('v=', $parse['query']);
                $videoId = end($result);
            }

            if (strpos($parse['path'], 'embed') == 1) {
                $result = explode('/', $parse['path']);
                $videoId = end($result);
            }
        }

        // Url is http://www.vimeo.com
        if (($parse['host'] == 'vimeo.com') || ($parse['host'] == 'www.vimeo.com')) {
            $videoType = 'vimeo';
            $videoId = ltrim($parse['path'], '/');
        }

        $hostNames = explode(".", $parse['host']);
        $rebuild = (!empty($hostNames[1]) ? $hostNames[1] : '') . '.' . (!empty($hostNames[2]) ? $hostNames[2] : '');

        // Url is an oembed url wistia.com
        if (($rebuild == 'wistia.com') || ($rebuild == 'wi.st.com')) {
            $videoType = 'wistia';
            if (strpos($parse['path'], 'medias') == 1) {
                $videoId = end(explode('/', $parse['path']));
            }
        }

        // If recognised type return video array
        if (!empty($videoType)) {
            $videoArray = array(
                'type' => $videoType,
                'id' => $videoId
            );
            return $videoArray;
        } else {
            return false;
        }
    }

    /**
     * Takes a Vimeo video/clip ID and calls the Vimeo API v2 to get the large thumbnail URL.
     *
     * @see https://support.advancedcustomfields.com/forums/topic/youtube-thumbnail-object-with-oembed-field/
     * @param string $clipId Clip ID for vimeo video
     * @return string|WP_Error
     */
    public static function getVimeoThumbnailUri(string $clipId)
    {
        $vimeoApiUrl = 'http://vimeo.com/api/v2/video/' . $clipId . '.php';
        $vimeoResponse = wp_remote_get($vimeoApiUrl);

        if (is_wp_error($vimeoResponse)) {
            return $vimeoResponse;
        } else {
            $vimeoResponse = unserialize($vimeoResponse['body']);
            return $vimeoResponse[0]['thumbnail_large'];
        }
    }

    /**
     * Takes a wistia oembed url and gets the video thumbnail url.
     *
     * @see https://support.advancedcustomfields.com/forums/topic/youtube-thumbnail-object-with-oembed-field/
     * @param string $videoUrl Url of video
     * @return string|WP_Error
     */
    public static function getWistiaThumbnailUri(string $videoUrl)
    {
        if (empty($videoUrl)) {
            return false;
        }

        $wistiaApiUrl = 'http://fast.wistia.com/oembed?url=' . $videoUrl;
        $wistiaResponse = wp_remote_get($wistiaApiUrl);

        if (is_wp_error($wistiaResponse)) {
            return $wistiaResponse;
        } else {
            $wistiaResponse = json_decode($wistiaResponse['body'], true);
            return $wistiaResponse['thumbnail_url'];
        }
    }
}

<?php

namespace TFA\Helpers;

/**
 * Generic WordPress related helper functions
 */
class WP
{
    /**
     * Add multiple filters to a closure
     *
     * @param $tags
     * @param $functionToAdd
     * @param int $priority
     * @param int $acceptedArgs
     * @see https://snippets.khromov.se/adding-multiple-actions-and-filters-using-anonymous-functions-in-wordpress/
     *
     * @return bool true
     */
    public static function addFilters($tags, $functionToAdd, $priority = 10, $acceptedArgs = 1)
    {
        //If the filter names are not an array, create an array containing one item
        if (!is_array($tags)) {
            $tags = (array) $tags;
        }

        //For each filter name
        foreach ($tags as $index => $tag) {
            add_filter($tag, $functionToAdd, (int) (is_array($priority) ? $priority[$index] : $priority), (int) (is_array($acceptedArgs) ? $acceptedArgs[$index] : $acceptedArgs));
        }

        return true;
    }

    /**
     * Add multiple actions to a closure
     *
     * @param $tags
     * @param $functionToAdd
     * @param int $priority
     * @param int $acceptedArgs
     * @see https://snippets.khromov.se/adding-multiple-actions-and-filters-using-anonymous-functions-in-wordpress/
     *
     * @return bool true
     */
    public static function addActions($tags, $functionToAdd, $priority = 10, $acceptedArgs = 1)
    {
        return static::addFilters($tags, $functionToAdd, $priority, $acceptedArgs);
    }

    /**
     * Copies function from WordPress, for use in front-end
     *
     * @see https://developer.wordpress.org/reference/functions/is_plugin_active/
     *
     * @param string $plugin
     * @return boolean
     */
    public static function isPluginActive(string $plugin)
    {
        return in_array($plugin, (array) get_option('active_plugins', []), true) || static::isPluginActiveForNetwork($plugin);
    }

    /**
     * Copies function from WordPress, for use in front-end
     *
     * @see https://developer.wordpress.org/reference/functions/is_plugin_active_for_network/
     *
     * @param string $plugin
     * @return boolean
     */
    public static function isPluginActiveForNetwork(string $plugin)
    {
        if (!is_multisite()) {
            return false;
        }

        $plugins = get_site_option('active_sitewide_plugins');
        if (isset($plugins[$plugin])) {
            return true;
        }

        return false;
    }
}

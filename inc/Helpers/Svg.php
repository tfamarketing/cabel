<?php

namespace TFA\Helpers;

use TFA\Factories\Cleaner;

/**
 * Helper class for Svg functions
 */
class Svg
{
    /**
     * Inlines svg/bodymovin json with viewBox hack and wraps external library
     *
     * @param string|int $key Filename or ID of svg file
     * @param array $classes Classes to attach to svg container & tag
     * @param bool $includeStyles Option to include inline style tag
     * @param string $htmlTag Change html tags used in output
     * @return string|bool
     */
    public static function inline($key, array $classes = [], bool $includeStyles = false, string $htmlTag = 'div')
    {
        switch(true) {
            case is_string($key) && in_array(substr($key, 0, 7), ['http://', 'https:/']):
                $key = attachment_url_to_postid($key);
            case is_int($key):
                $key = get_attached_file($key);
                break; 
        } 
        $svgHtml = '';
        $path = pathinfo($key);
        $dirName = $path['dirname'] ?? '';
        $fileName = $path['filename'] ?? '';
        $extension = $path['extension'] ?? 'svg';
        $svgDir = file_exists($key) ? $dirName : Path::getThemePath($dirName);
        if ((!empty($svgDir)) && (!empty($fileName)) && ($extension == 'svg' || $extension == 'json') && file_exists("$svgDir/$fileName.$extension")) {
            $classes = array_merge(array_values($classes), [$fileName]);
            $widthType = in_array('max-width', $classes) ? 'max-width' : 'width';
            $classes = implode(' ', $classes);

            if ($extension != 'json') {
                $icons = \InlineSvg\Collection::fromPath($svgDir);
                $icons->addTransformer(Cleaner::getInstance());

                if (!$includeStyles) {
                    $icons->addTransformer(function ($elements) {
                        foreach ($elements->getElementsByTagName('style') as $element) {
                            $element->parentNode->removeChild($element);
                        }
                        return $elements;
                    });
                }

                $svgAtts = [
                    'class' => $classes,
                    'preserveAspectRatio' => 'xMidYMin meet',
                ];

                $object = $icons->get($fileName)->withAttributes($svgAtts);

                $height = $object->get()->getAttribute('height');
                $width = $object->get()->getAttribute('width');

                if (empty($width) && empty($height)) {
                    $viewBox = $object->get()->getAttribute('viewBox');
                    $viewBox = (!empty($viewBox)) && is_string($viewBox) ? explode(' ', $viewBox, 4) : [];
                    if (!empty($viewBox)) {
                        $viewBox = array_values(array_filter($viewBox, function ($number) {
                            return (int) $number !== 0;
                        }));
                    }
                } else {
                    $viewBox = [$width, $height];
                }

                $htmlTag = ($htmlTag ?: 'div');

                if (($viewBox[0] ?? false) && ($viewBox[1] ?? false)) {
                    $viewBoxCalc = 100 * ($viewBox[1] / $viewBox[0]);
                    $svgHtml .=
                        "<{$htmlTag} class=\"scaling-svg-container $classes\" style=\"$widthType: {$viewBox[0]}px;\">
                    <{$htmlTag} class=\"scaling-svg-inner\" style=\"padding-bottom: $viewBoxCalc%;\">
                    $object
                    </{$htmlTag}>
                </{$htmlTag}>";
                } else {
                    $svgHtml = $object;
                }
            } else {
                $classes .= ' animated';

                $svgHtml .=
                    "<{$htmlTag} class=\"scaling-svg-container $classes\">
                <{$htmlTag} class=\"scaling-svg-inner\">
                    <{$htmlTag} class=\"svg $classes\" data-json=\"$key\"></{$htmlTag}>
                </{$htmlTag}>
            </{$htmlTag}>";
            }
        }

        return empty($svgHtml) ? false : $svgHtml;
    }
}

<?php

namespace TFA\Helpers;

use CaseConverter\CaseString;

/**
 * Helper class for generic PHP functions
 */
class Generic
{
    /**
     * Change keys in array for new ones
     *
     * @param array $array - Array to change
     * @param array|string $old_keys - Keys to be replaced
     * @param array|string $new_keys - Keys that will replace
     * @return void
     */
    public static function changeArrayKeys(array $array, $old_keys, $new_keys)
    {
        if (empty($array) || empty($old_keys) || empty($new_keys)) {
            return $array;
        }

        $old_keys = (array) $old_keys;

        foreach ($old_keys as $key) {
            if (array_key_exists($key, $array)) {
                return $array;
            }
        }

        $new_keys = (array) $new_keys;

        $key_replace_array = [];

        if (count($old_keys) !== count($new_keys)) {
            return $array;
        }

        $key_replace_array = array_combine($old_keys, $new_keys);

        $keys = array_keys($array);

        foreach ($key_replace_array as $old_key => $new_key) {
            $keys[array_search($old_key, $keys)] = $new_key;
        }

        return array_combine($keys, array_values($array));
    }

    /**
     * Changes case of array keys using `CaseString()`
     *
     * @param array $array
     * @param string $originalCase
     * @param string $newCase
     * @see https://github.com/joachim-n/case-converter For case types that can be used
     * @return array
     */
    public static function changeCaseArrayKeys(array $array, string $originalCase, string $newCase)
    {
        if ((!empty($array)) &&
            (!(method_exists(CaseString::class, $originalCase) &&
                method_exists(CaseString::class, $newCase)))
        ) {
            return [];
        }

        foreach ($array as $key => $value) {
            $storedKey = $key;
            if (is_array($value)) {
                $value = static::changeCaseArrayKeys($value, $originalCase, $newCase);
            }
            if (is_string($key)) {
                $storedKey = CaseString::{$originalCase}($storedKey)->{$newCase}();
            }
            unset($array[$key]);
            $array[$storedKey] = $value;
        }

        return $array;
    }

    /**
     * Check whether array keys are strings throughout the array
     *
     * @param array $array
     * @return bool
     */
    public static function checkArrayStringKeys(array $array)
    {
        if (count(array_filter(array_keys($array), 'is_string')) > 0) {
            return true;
        }
        return false;
    }

    /**
     * Prefixes array keys
     *
     * @param array $array
     * @param string $prefix
     * @return array
     */
    public static function prefixArrayKeys(array $array, string $prefix)
    {
        if (!static::checkArrayStringKeys($array)) {
            return [];
        }
        return array_combine(
            array_map(
                function ($key) use ($prefix) {
                    return "$prefix$key";
                },
                array_keys($array)
            ),
            $array
        );
    }

    /**
     * Creates array from files recursively
     *
     * @param string $dir
     * @param array $results Keys are filenames without extension, values are the paths
     * @param bool $relativePath Sets whether paths should be relative to the theme dir root
     * @param string $subDirReplaceCharacter Sets replacement character for directory seperator
     * @return array
     */
    public static function createArrayFromFiles(string $dir, bool $relativePath = true, string $subDirReplaceCharacter = '-')
    {
        $results = [];
        if (!empty($dir)) {
            $themeDir = Path::getThemePath();
            $dir = file_exists($dir) ? dirname($dir) : $themeDir . DIRECTORY_SEPARATOR . ltrim($dir, '/\\');
            if ((!empty($dir)) && is_dir($dir = realpath($dir))) {
                $dirIt = new \RecursiveDirectoryIterator($dir);
                $dirIt->setFlags(\RecursiveDirectoryIterator::SKIP_DOTS);
                $it = new \RecursiveIteratorIterator($dirIt);
                $subDirReplaceCharacter = ($subDirReplaceCharacter ?: '-');

                foreach ($it as $file) {
                    $filePath = $file->getPathname();

                    $fileBaseKey = '';
                    if (strpos($filePath, $dir) !== false) {
                        $fileBaseKey .= ltrim(substr($filePath, strlen($dir)), '/\\');
                        $fileBaseKey = str_replace(['/', '\\', DIRECTORY_SEPARATOR], $subDirReplaceCharacter, $fileBaseKey);
                        if (!empty($fileExt = $file->getExtension())) {
                            $fileBaseKey = preg_replace('/.' . $fileExt . '$/', '', $fileBaseKey);
                        }
                    }

                    $filePath = $relativePath ? Path::getRelativePath($filePath) : $filePath;

                    if (!empty($fileBaseKey)) {
                        $results[$fileBaseKey] = $filePath;
                    }
                }
            }
        }
        return $results;
    }

    /**
     * Convert a multi-dimensional array into a single-dimensional array.
     * @author Sean Cannon, LitmusBox.com | seanc@litmusbox.com
     * @param  array $array The multi-dimensional array.
     * @return array
     */
    public static function arrayFlatten(array $array)
    {
        if (!is_array($array)) {
            return false;
        }
        $result = [];
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, static::arrayFlatten($value));
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    /**
     * Wrapper around `file_get_contents()`
     *
     * Performs validation check before returning to check `$file` is a file and to use `locate_template()` to get the full path
     *
     * @param string $file File path
     * @return bool|string
     */
    public static function getFile(string $file)
    {
        $file = file_exists($file) ? $file : Path::getThemePath($file);
        if ((!empty($file)) && (!is_dir($file))) {
            $file = file_get_contents($file);
            return $file;
        }
        return false;
    }

    /**
     * Wraps `json_decode()` & returns array
     *
     * Performs validation check before returning to ensure `$json` is valid
     *
     * @param string $json Json string
     * @return array
     */
    public static function getJsonArray(string $json)
    {
        $json = json_decode($json, true);
        if ((!empty($json)) && (json_last_error() == JSON_ERROR_NONE)) {
            return $json;
        }
        return [];
    }

    /**
     * Check file has changed by creating hash file
     *
     * @param string $file File path
     * @param array $args Additional arguments
     * @return string|bool
     */
    public static function checkFileChange(string $file, array $args = [])
    {
        if (!empty($file) && file_exists($file) && !is_dir($file)) {
            $dir = (($args['dir'] ?? '') ?: pathinfo($file, PATHINFO_DIRNAME));
            $dir = rtrim($dir, '/\\');
            $handle = (($args['handle'] ?? '') ?: pathinfo($file, PATHINFO_FILENAME));

            $hashFileLocation = "$dir/$handle-hash.txt";
            $lastChanged = (file_get_contents($hashFileLocation) ?: '');
            if (!file_exists($hashFileLocation) || $lastChanged != ($newHash = md5(filemtime($file)))) {
                file_put_contents($hashFileLocation, $newHash);
                return ($args['returnHash'] ?? '') ? $newHash : true;
            }
            return ($args['returnHash'] ?? '') ? $lastChanged : false;
        }
        return false;
    }

    /**
     * Removes all line breaks from a string
     *
     * Wraps `str_replace()`
     *
     * @param string $string
     * @return string
     */
    public static function stripLineBreaks(string $string)
    {
        if (!empty($string)) {
            return str_replace(["\r", "\n"], '', $string);
        }
        return '';
    }

    /**
     * Wraps `preg_match()` and extracts src attribute from html string
     *
     * @param string $html
     * @return string|bool
     */
    public static function getSrcFromHtml(string $html)
    {
        if (!empty($html)) {
            preg_match('/src="(.+?)"/', $html, $matches);
            return ($matches[1] ?? '') ?: false;
        }
        return false;
    }

    /**
     * Replace array key with new one recursively
     *
     * @param array $array
     * @param string $oldKey
     * @param string $newKey
     * @return array
     */
    public static function replaceArrayKeys(array $array, string $oldKey, string $newKey)
    {
        $return = [];
        foreach ($array as $key => $value) {
            if ($oldKey == $key) {
                $key = $newKey;
            }

            if (is_array($value)) {
                $value = static::replaceArrayKeys($value, $oldKey, $newKey);
            }

            $return[$key] = $value;
        }
        return $return;
    }
}

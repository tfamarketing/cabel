<?php

namespace TFA\Traits;

/**
 * Trait to use in factory classes to prepare keys for Auryn injector instance
 */
trait Prepare_Keys_For_Injector
{
    /**
     * Prepares argument array keys for Auryn Injector
     *
     * Used with factory classes
     *
     * @param array $args
     * @return array
     */
    public static function prepareKeys(array $args)
    {
        if (!empty($args)) {
            $argsKeys = array_map(function ($key) {
                if (is_string($key)) {
                    return ":{$key}";
                }
                return $key;
            }, array_keys($args));

            return array_combine($argsKeys, array_values($args));
        }
        return [];
    }
}

<?php // do_shortcode('[button text="test" link="https://google.com" style="2"]'); ?> 

<section class="column-banner">
	<div class="cd-projects-container">
		<ul class="cd-projects-previews"> 
            <?php foreach($columns as $column): ?>
                <li> 
                    <div class="stripes"></div>
                    <div class="arrows"></div>
                    <a href="#<?= $column['title'] ?>">
                        <span class="colour-bg h-100 w-100 position-absolute bg-<?= $column['backgroundColor'] ?>"></span>
                        <div class="cd-project-title is-<?= $column['backgroundColor'] ?>">
                            <h2><?= $column['title'] ?></h2>  
                        </div>
                    </a>
                    <span class="img-bg h-100 w-100 position-absolute" style="background-image: url(<?= $column['backgroundImage']['url'] ?>);"></span>
                </li>
            <?php endforeach; ?>
		</ul> 
        
        <?php if (have_rows('columns')): ?>
		<ul class="cd-projects"> 
            <?php while (have_rows('columns')): the_row(); ?>
                <li> 
                    <div class="preview-image" style="background-image: url(<?= get_sub_field('background_image')['url'] ?>);">
                        <div class="cd-project-title">
                            <h2><?= get_sub_field('title') ?></h2>  
                        </div> 
                    </div>

                    <div class="cd-project-info d-block">
                        <?= TFA\Helpers\ACF::getFlexibleLayouts('column_content'); ?>
                    </div> 
                </li>   
            <?php endwhile; ?>
        </ul>
        <?php endif; ?>

		<button class="scroll cd-text-replace">Scroll</button>
	</div> 
</section>
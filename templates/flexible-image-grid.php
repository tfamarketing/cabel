<section class="image-grid">
    <div class="row no-gutters">
        <?php foreach($gridItems as $gridItem): ?>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="grid-column w-100">
                    <a href="<?= $gridItem['link']['url'] ?>" class="grid-item-header position-relative" style="background-image: url('<?= $gridItem['image']['url'] ?>')">
                        <div class="grid-item-overlay"></div>
                        <?= $gridItem['title'] ?>
                        <span class="plus"><span>
                    </a>
                    <div class="d-none d-lg-block">
                        <?php foreach($gridItem['galleryItems'] as $item): ?>
                            <div href="<?= $gridItem['link']['url'] ?>" class="grid-item position-relative" style="background-image: url('<?= $item['image']['url'] ?>')">
                                <span class="bg-green grid-item-overlay"></span>
                                <h5 class="txt-white"><?= $item['title'] ?></h4>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>
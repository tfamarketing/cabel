<?php
$fullWidth = (bool) ($fullWidth ?? false);
$bgColour = $bgColour ?? 'primary';
$textColour = $textColour ?? false;
$url = $url ?? '#';
$content = $content ?? false;
$target = $target ?? '_self';
?>

<?php if ($content): ?>
<a href="<?=esc_url($url)?>" target="<?=$target?>" class="shortcode-btn btn btn-<?=$bgColour?><?php if ($textColour): ?> text-<?=$textColour?><?php endif; ?><?php if ($fullWidth): ?> w-100<?php endif; ?>"><?=wp_strip_all_tags($content, true)?></a>
<?php endif; ?>

<?php
    $id = $id ?? false;
    $url = $url ?? false;
    $target = $target ?? '_blank';
    $fileNameWExt = $fileNameWExt ?? false;
    $classes = $classes ?? false;
?>

<?php if ($url || $fileNameWExt): ?>
<a href="<?=esc_url($url)?>" target="<?=$target?>" class="attachment-link<?php if ($classes): ?> <?=$classes?><?php endif; ?>" style="color: inherit;"><?=$fileNameWExt ?? $url?></a>
<?php endif; ?>

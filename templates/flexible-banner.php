

<?php if(!empty($title)): ?>
    <section class="banner bg-<?= $backgroundColor ?>" <?= empty($backgroundImage['url'])?'':'style="background-image: url('.$backgroundImage['url'].')"';?>>
        <div class="lines"></div>
        <div class="circle"></div>
        <div class="green-arrows"></div>
        <div class="white-arrows"></div>
        <div class="dashes"></div>
        <div class="container">
            <?php if(!empty($character['url'])): ?>
                <img class="sprite position-absolute" src="<?= $character['url'] ?>" alt="<?= $character['alt'] ?>">
            <?php endif; ?>
            <div class="row">
                <div class="col-12 position-relative d-flex align-items-center justify-content-center">
                    <div class="position-relative">
                        <div class="arrow">
                            <?php include_arrow('white', 'solid'); ?>
                        </div>
                        <h1 class="txt-white"><?= $title ?></h1>
                        <div class="arrow">
                            <?php include_arrow('green', 'hollow') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
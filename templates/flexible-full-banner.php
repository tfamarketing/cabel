<?php
use TFA\Helpers\Svg;
use TFA\Helpers\Path;


$fields = get_fields();
?>
<section class="full-banner">

    <video id="cabel-video" playsinline="true" autoplay="true" muted="muted" width="100%" height="auto">
        <source src="<?= $fields['main_content'][0]['full_banner_video']; ?>" type="video/mp4">
    </video>

    <div class="banner-img">
        <div class="banner-logo">
            <?= Svg::inline('img/cabel_logo_w_deco.svg') ?>
        </div>

        <div class="banner-arrow">
            <?= Svg::inline('img/home_banner_arrow.svg') ?>
        </div>
    </div>
    
</section>
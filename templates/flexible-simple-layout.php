<?php $basicContent = ['has_image', 'has_content', 'has_button']; ?>

<?php $type = 'coloured';
if(!empty($backgroundImage['url'])) $type = 'image'; ?>

<section class="simple-layout <?= $type ?> bg-<?= $backgroundColor ?>" <?= ($type==='image')?'style="background-image: url('.$backgroundImage['url'].')"':''; ?>">
    <div class="container">
        <div class="row">
            <?php if($centerColumn && checkEmptyArray($centerColumn, $basicContent)): ?>
                <div class="col-12 d-flex align-items-center">
                    <?php include_component('basic-content', $centerColumn); ?>
                </div>
            <?php endif; ?>
            <?php if($leftColumn && checkEmptyArray($centerColumn, $basicContent)): ?>
                <div class="col-lg-6 col-12 d-flex align-items-center">
                    <?php include_component('basic-content', $leftColumn); ?>
                </div>
            <?php endif; ?>
            <?php if($rightColumn && checkEmptyArray($centerColumn, $basicContent)): ?>
                <div class="col-lg-6 col-12 d-flex align-items-center">
                    <?php include_component('basic-content', $rightColumn); ?>
                </div>
            <?php endif; ?> 
        </div>
    </div>
</section>
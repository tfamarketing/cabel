<?php 
$info = get_field('information', 'options'); 
?>

<section class="contact-form bg-white">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="content">
                    <?= $title ?>
                </div>
                <div class="form">
                    <div class="row no-gutters">
                        <div class="col-lg col-12">
                            <div class="contact">
                                <h3>How can we help?</h3>
                                <?= do_shortcode($shortcode) ?>
                            </div>
                        </div>
                        <?php if($showInfo): ?>
                            <div class="col-lg-auto col-12">
                                <div class="bg-navygreen info">
                                    <h3 class="txt-green">Find Us</h3>
                                    <b><h6><?= $info['company_name'] ?></h6></b>
                                    <br/>
                                    <h6 class="address"><?php $i=0; foreach($info['address'] as $add): $i++; ?>
                                        <?= $add . (count($info['address']) <= $i ? ' ':', '); ?> 
                                    <?php endforeach; ?></h6>
                                    <br/> 
                                    <b><h6>Telephone</h6></b>
                                    <h6><?= empty($info['telephone_1']['displayed'])?$info['telephone_1']['formatted']:$info['telephone_1']['displayed'] ?></h6>
                                    <br/>
                                    <b><h6>Out Of Hours</h6></b>
                                    <h6><?= empty($info['telephone_2']['displayed'])?$info['telephone_2']['formatted']:$info['telephone_2']['displayed'] ?></h6>
                                    <br/>
                                    <b><h6>Email</h6></b>
                                    <h6><?= $info['links']['email'] ?></h6>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</section>
<?php // $content 
$img = $content['img']; ?>

<?php if(!empty($img['url'])): ?>

    <?php $justify = 'justify-content-';
    switch($content['align']){
        case 'left': $justify .= 'start '; break;
        case 'right': $justify .= 'end '; break; 
        default: $justify .= 'center '; break;
    } ?>

    <?php if(in_array($content['style'], ['semi', 'full'])) $style = $content['style'].' '; ?>

    <div class="d-flex align-items-center <?= $justify.$style ?>image-wrapper position-relative">
        <?= TFA\Helpers\Svg::inline(get_template_directory().'/img/lines/skewed-grey-lines.svg', ['skewed-grey-lines']); ?> 
        <img src="<?= $img['url'] ?>" <?= empty($img)?'': 'alt="'.$img['alt'].'"'; ?>/> 
        <span class="deco">
            <?php include_arrow('green', 'solid'); ?>
            <?php include_arrow('grey', 'hollow'); ?>
            <?= TFA\Helpers\Svg::inline(get_template_directory().'/img/lines/circle-green-lines.svg', ['circle-green-lines']); ?>
        </span>
    </div>

<?php endif; ?>
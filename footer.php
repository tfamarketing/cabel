<?php 
$info = get_field('information', 'options'); 
$footer = get_field('footer', 'options'); ?>
			<footer class="footer">
				<div class="container">
					<div class="row">
						<div class="col-md-auto col-12 align-items-center justify-content-center d-md-block d-flex">
							<div class="logo-wrapper">
								<div class="logo">
									<a href="<?= home_url(); ?>"> 
										<img src="<?= wp_get_attachment_image_src(get_theme_mod( 'custom_logo' ),'full')[0] ?>" alt="Site Logo">
									</a>
								</div>
								<div class="social-icons row no-gutters"> 
									<?php if(!empty($info['links']['facebook'])): ?>
										<div class="col-3">
											<a href="<?= $info['links']['facebook'] ?>">
												<i class="fab fa-facebook-f"></i>
											</a>
										</div>
									<?php endif; 
									if(!empty($info['links']['instagram'])): ?>
										<div class="col-3">
											<a href="<?= $info['links']['instagram'] ?>">
												<i class="fab fa-instagram"></i>
											</a>
										</div>
									<?php endif;  
									if(officeOpen()) {
										$formatNum = empty($info['telephone_1']['formatted'])?$info['telephone_1']['displayed']:$info['telephone_1']['formatted']; 
									} else {
										$formatNum = empty($info['telephone_2']['formatted'])?$info['telephone_2']['displayed']:$info['telephone_2']['formatted']; 
									}
									if(!empty($formatNum)): ?>
										<div class="col-3">
											<a class="icon" href="tel:<?= $formatNum ?>">
												<i class="fas fa-phone"></i>
											</a>
										</div>
									<?php endif;
									if(!empty($info['links']['email'])): ?>
										<div class="col-3">
											<a href="<?= $info['links']['email'] ?>">
												<i class="fas fa-envelope"></i>
											</a>
										</div>
									<?php endif; ?> 
								</div>
							</div>
						</div>
						<div class="col-md col-12">
							<div class="row justify-content-between">
								<?php 
								$tel1 = (!empty($info['telephone_1']['formatted'])||!empty($info['telephone_1']['displayed']));
								$tel2 = (!empty($info['telephone_2']['formatted'])||!empty($info['telephone_2']['displayed']));
								if($tel1 || $tel2): ?>
									<div class="col-sm-3 col-12">
										<div class="item">
											<?php if($tel1): ?>
												<div class="tel">
													<h3>Working Hours</h3>
													<a href="tel:<?= empty($info['telephone_1']['formatted'])?$info['telephone_1']['displayed']:$info['telephone_1']['formatted'] ?>">
														<p><?= empty($info['telephone_2']['displayed'])?$info['telephone_2']['formatted']:$info['telephone_2']['displayed'] ?></p>
													</a>
												</div>
											<?php endif; ?>
											<?php if($tel2): ?> 
												<div class="tel">
													<h3>Out Of Hours</h3>
													<a href="tel:<?= empty($info['telephone_2']['formatted'])?$info['telephone_2']['displayed']:$info['telephone_2']['formatted'] ?>">
														<p><?= empty($info['telephone_2']['displayed'])?$info['telephone_2']['formatted']:$info['telephone_2']['displayed'] ?></p>
													</a> 
												</div>
											<?php endif; ?>
										</div>
									</div>
								<?php endif;  
								if($addr = (!empty($info['address']['l1']) || !empty($info['address']['l2']) || !empty($info['address']['l3']) || !empty($info['address']['l4'])) || !empty($info['company_name'])): ?>
									<div class="col-sm-3 col-12">
										<div class="item">
											<?php if(!empty($info['company_name'])): ?>
												<h3><?= $info['company_name'] ?></h3>
											<?php endif; ?>
											<?php if($addr): ?>
												<p class="mb-0"><?= $info['address']['l1'] ?></p>
												<p class="mb-0"><?= $info['address']['l2'] ?></p>
												<p class="mb-0"><?= $info['address']['l3'] ?></p>
												<p class="mb-0"><?= $info['address']['l4'] ?></p>
											<?php endif; ?>
										</div>
									</div>
								<?php endif; ?>
								<?php $locations = get_nav_menu_locations();
								$menu1 = wp_get_nav_menu_items($locations['tfa-footer-menu-1']);   
								if($menu1 !== false): ?>
									<div class="col-sm-3 col-12">
										<h3>NAVIGATION</h3>
										<?php foreach($menu1 as $item): ?>
											<a href="<?= $item->url ?>">
												<p><?= $item->title ?></p>
											</a>
										<?php endforeach; ?>
									</div>
								<?php endif; 
								$menu2 = wp_get_nav_menu_items($locations['tfa-footer-menu-2']);  
								if($menu2 !== false): ?>
									<div class="col-sm-3 col-12">
										<?php if($menu1): ?>
												<h3 class="d-sm-block d-none">&nbsp;</h3>
											<?php else: ?>
												<h3>NAVIGATION</h3>
										<?php endif; ?>

										<?php foreach($menu2 as $item): ?>
											<a href="<?= $item->url ?>">
												<p><?= $item->title ?></p>
											</a>
										<?php endforeach; ?>
									</div>
								<?php endif; ?>
							</div>
						</div>
						<?php if(!empty($footer['images'][0]['image']['url'])): ?>
							<div class="col-12">
								<div class="footer-bottom row no-gutters align-items-center justify-content-between">
									<?php foreach($footer['images'] as $image): ?> 
										<img src="<?= $image['image']['url'] ?>" alt="<?= $image['image']['alt'] ?>"/> 
									<?php endforeach; ?>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</footer>
			<div class="tfa">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<a href="<?= 'https://www.t-f-a.co.uk/' ?>">
								<h3>WEBSITE BY <b>TFA</b></h3>
							</a>
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>
	</body>
</html>

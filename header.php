<?php use TFA\Helpers\Navigation; ?>

<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<link rel="stylesheet" href="https://use.typekit.net/ykt4pdn.css">
		<script src="https://kit.fontawesome.com/d6eaecf138.js" crossorigin="anonymous"></script>

		<?php wp_head(); ?>

	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="wrapper bg-white"> 
			<div class="header-spacer"></div>
			<!-- header -->
			<header class="header clear d-flex position-fixed align-items-center bg-white" role="banner">
				
					<div class="logo">
						<a href="<?= home_url(); ?>"> 
							<img src="<?= wp_get_attachment_image_src(get_theme_mod( 'custom_logo' ),'full')[0] ?>" alt="Site Logo">
						</a>
					</div>

					<div class="info d-flex align-items-center justify-self-end">
						<?php $info = get_field('information', 'options'); ?>
						<?php if(!empty($info['links']['email'])): ?>
							<a class="d-none d-lg-block" href="mailto:<?= $info['links']['email'] ?>">
								<i class="fas fa-envelope"></i>
								<?= $info['links']['email'] ?>
							</a>
						<?php endif; ?>

						<?php if(officeOpen()) {
							$formatNum = empty($info['telephone_1']['formatted'])?$info['telephone_1']['displayed']:$info['telephone_1']['formatted'];
							$displayNum = empty($info['telephone_1']['displayed'])?$info['telephone_1']['formatted']:$info['telephone_1']['displayed'];
						} else {
							$formatNum = empty($info['telephone_2']['formatted'])?$info['telephone_2']['displayed']:$info['telephone_2']['formatted'];
							$displayNum = empty($info['telephone_2']['displayed'])?$info['telephone_2']['formatted']:$info['telephone_2']['displayed'];
						}
						if(!empty($formatNum) && !empty($displayNum)): ?>
							<a class="d-none d-lg-block" href="tel:<?= $formatNum ?>">
								<i class="fas fa-phone"></i>
								<?= $displayNum ?>
							</a>
						<?php endif; ?>

						<?php if(!empty(($service = get_field('header', 'options'))['link']['url']) || !empty($service['text'])): ?>
							<a class="d-none service d-lg-block" <?= empty($service['link']['url'])?'': 'href="'.$service['link']['url'].'"'; ?>> 
								<i class="fas fa-history"></i>
								<?= $service['text']; ?>
							</a>
						<?php endif; ?>

						<div class="burger">
							<span></span>
							<span></span>
							<span></span>
							<span></span>
							<span></span>
						</div>
					</div>

					<div class="nav-overlay"></div>

					<nav class="navigation">
						<div class="close-btn"></div>
						
						<?= Navigation::getMenu('tfa-main-menu', ['walker' => '']); ?>

						<div class="info">
							<?php if(!empty(($service = get_field('header', 'options'))['link']['url']) || !empty($service['text'])): ?>
								<a class="d-block service d-lg-none" <?= empty($service['link']['url'])?'': 'href="'.$service['link']['url'].'"'; ?>> 
									<i class="fas fa-history"></i>
									<?= $service['text']; ?>
								</a>
							<?php endif; ?>
						</div>
					</nav>

					<div class="quick-contact">

						<div class="quick-contact-wrapper">
							<h2>24 Hour Service</h2>

							<div class="inner-wrapper">
								<div class="contact-info">
									<div class="phone-numbers">
										<i class="fas fa-phone-alt"></i>
										<div class="number">
											<p class="title">Working Hours</p>
											<a href="tel:01572 755168">01572 755168</a>
										</div>
										<div class="number">
											<p class="title">Out of Hours</p>
											<a href="tel:07956 537246">07956 537246</a>
										</div>
									</div>
									<div class="email">
										<i class="fas fa-envelope"></i>
										<p class="title">Info@cabel.co.uk</p>
									</div>
								</div>
								<div class="contact-form">
									<h3>Quick Contact</h3>
									<!-- Local Shortcode -->
									<!-- <?= do_shortcode('[contact-form-7 id="177" title="Quick Contact"]'); ?> -->

									<!-- TestServer Shortcode -->
									<?= do_shortcode('[contact-form-7 id="165" title="Quick Contact"]'); ?>
								</div>
							</div>
						</div>
					</div>
					
			</header>
			<!-- /header -->
			

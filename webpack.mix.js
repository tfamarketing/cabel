let mix = require('laravel-mix'),
    exporter = require('sass-export').exporter,
    path = require('path');
require('laravel-mix-modernizr');
require('laravel-mix-polyfill');
const fs = require("fs");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

let node_env = process.env.NODE_ENV;

let devtool_mode = 'none';
let maps = false;
if (node_env === 'production') {
    devtool_mode = 'none';
    maps = true;
} else if (node_env === 'development') {
    devtool_mode = 'inline-source-map';
} else {
    devtool_mode = 'none';
}

mix
    .setPublicPath('./')
    .webpackConfig({
        devtool: devtool_mode
    })
    .sass('src/styles/app.scss', 'dist/styles.css')
    .sass('src/styles/admin/app.scss', 'dist/admin.css')
    .options({
        postCss: [
            require('autoprefixer')
        ]
    })
    .polyfill()
    .js('src/scripts/main.js', 'dist/scripts.js')
    .js('src/scripts/load-more-posts.js', 'dist/load-more-posts.js')
    .version()
    .modernizr()
    .sourceMaps(maps)
    .extract()
    .webpackConfig({
        externals: {
            "jquery": "jQuery"
        }
    })
    .autoload({
        jquery: ['$', 'window.jQuery', 'jQuery']
    })
    .options({
        processCssUrls: false, // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
    })
    .then((stats) => {
        let compilation = stats.compilation, // Get compliation object
            outputPath = compilation.compiler.options.output.path, // Get outpath path (root path of theme)
            assets = Array.from(compilation.fileDependencies); // Get all file deps & convert to array

        if (stats && compilation && assets) {

            // Filter assets array to only include scss files
            assets = assets.filter((asset) => {
                let ext = path.extname(asset);
                return ext == '.scss';
            });

            // Filter assets array for inclusion
            let exportAssets = assets.filter((asset) => {
                let basename = path.basename(asset), // Get basename of file
                    relPath = asset.replace(outputPath, ''), // Get relative path of asset
                    isNodeDep = relPath.startsWith(path.sep + 'node_modules'); // Check if asset path is a npm dep

                // Remove underscore from base name
                if (basename.startsWith('_')) {
                    basename = basename.substr(1);
                }

                // Return non npm paths & paths that start with export prefix
                return (
                    !isNodeDep &&
                    (
                        basename.startsWith('export-' ||
                            basename.startsWith('export_')
                        )
                    )
                );
            });

            // Create array of asset paths
            let assetPaths = assets.map(asset => path.dirname(asset));
            assetPaths = Array.from(new Set(assetPaths).values()); // Remove duplicate values

            // Initalize exporter
            let exporterObject = exporter({
                    inputFiles: exportAssets,
                    includePaths: assetPaths
                }),
                structuredData = exporterObject.getStructured(); // Get structured array of objects

            if (structuredData) {
                let jsonOutput = JSON.stringify(structuredData, null, 2); // Convert to json string
                fs.writeFileSync(path.normalize(outputPath + '/dist/exported-scss.json'), jsonOutput); // Write to file
            }
        }
    });
<?php // Template Name: Flexible Page ?>

<?php get_header(); ?>

	<main role="main" class="<?= !is_front_page()?'':'front-page' ?>">
		<?= TFA\Helpers\ACF::getFlexibleLayouts('main_content'); ?>
	</main> 

<?php get_footer(); ?>
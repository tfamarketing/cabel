require('Modernizr');
require('bootstrap');
require('slick-carousel');

// Cody-House
jQuery(document).ready(function($){
	//cache DOM elements
	var projectsContainer = $('.cd-projects-container'),
		projectsPreviewWrapper = projectsContainer.find('.cd-projects-previews'),
		projectPreviews = projectsPreviewWrapper.children('li'),
		projects = projectsContainer.find('.cd-projects'),
		navigationTrigger = $('.cd-nav-trigger'),
		navigation = $('.cd-primary-nav'),
		//if browser doesn't support CSS transitions...
		transitionsNotSupported = ( $('.no-csstransitions').length > 0);

	var animating = false,
		//will be used to extract random numbers for projects slide up/slide down effect
		numRandoms = projects.find('li').length, 
		uniqueRandoms = [];

	//open project
	projectsPreviewWrapper.on('click', 'a', function(event){
		event.preventDefault();
		if( animating == false ) {
			animating = true;
			navigationTrigger.add(projectsContainer).addClass('project-open');
			$('.burger').addClass('banner-toggle');
			openProject($(this).parent('li'));
		}
	});

	navigationTrigger.on('click', function(event){
		event.preventDefault();
		
		if( animating == false ) {
			animating = true;
			if( navigationTrigger.hasClass('project-open') ) {
				//close visible project
				navigationTrigger.add(projectsContainer).removeClass('project-open');
				closeProject();
			} else if( navigationTrigger.hasClass('nav-visible') ) {
				//close main navigation
				console.log('test');
				navigationTrigger.removeClass('nav-visible');
				navigation.removeClass('nav-clickable nav-visible');
				if(transitionsNotSupported) projectPreviews.removeClass('slide-out');
				else slideToggleProjects(projectsPreviewWrapper.children('li'), -1, 0, false);
			} else {
				//open main navigation
				console.log('test2');
				navigationTrigger.addClass('nav-visible');
				navigation.addClass('nav-visible');
				if(transitionsNotSupported) projectPreviews.addClass('slide-out');
				else slideToggleProjects(projectsPreviewWrapper.children('li'), -1, 0, true);
			}
		}	

		if(transitionsNotSupported) animating = false;
	});

	//scroll down to project info
	projectsContainer.on('click', '.scroll', function(){
		projectsContainer.animate({'scrollTop':$(window).height()}, 500); 
	});

	//check if background-images have been loaded and show project previews
	projectPreviews.children('a').bgLoaded({
	  	afterLoaded : function(){
			   showPreview(projectPreviews.eq(0));
	  	}
	});

	if ($('body.home').length) {
		var bannerOverlay = $('.full-banner .banner-img'),
			fadeInTime = 18000;

		$(window).on('resize load', function() {
			if ($(window).width() > 1199.98) {
				setTimeout(() => {
					console.log('Apply change')
					bannerOverlay.addClass('element-visible');
					// bodymovin.loadAnimation({
					// 	container: document.getElementById('mouse-anim'),
					// 	renderer: 'svg',
					// 	loop: true,
					// 	autoplay: true,
					// 	path: 'wp-content/themes/guestkey/svg/animation/data.json'
					// })
				}, fadeInTime);
			}
		})	
	}

	// Opens nav or closes banner columns
	$('.burger').click(function(){
		if($(this).hasClass('banner-toggle') && animating == false) {
			$(this).removeClass('banner-toggle');
			projectsContainer.removeClass('project-open');
			closeProject();
			$('[role=main]').height('auto')
		} else if($('.quick-contact').hasClass('open')) {
			$(this).toggleClass('toggled');
			$('.quick-contact').toggleClass('open')
			$('.service').toggleClass('active');
		} else {
			$(this).toggleClass('toggled');
			$('nav.navigation').toggleClass('open');
			$('.nav-overlay').toggleClass('menu-open');
		}
	});

	$('.close-btn').on('click', function() {
		$('.burger').toggleClass('toggled');
		$('nav.navigation').toggleClass('open');
		$('.nav-overlay').toggleClass('menu-open');
	})

	$('.service').on('click', function(e) {
		e.preventDefault();

		if ($('nav.navigation').hasClass('open')) {
			$('nav.navigation').removeClass('open');
			$('.nav-overlay').removeClass('menu-open');

			setTimeout(() => {
				$('.quick-contact').toggleClass('open');
				$(this).toggleClass('active');
			}, 200);
		} else {
			$('.quick-contact').toggleClass('open');
			$('.burger').toggleClass('toggled');
			$(this).toggleClass('active');
		}
	})

	$(window).on('resize.bodyHeight sliderPos', function() {

		console.log('resize')
		if ($('.burger').hasClass('banner-toggle')) {
			setBodyHeight();
		}
	})

	$('.banner-arrow').on('click', function() {
		var parent = $(this).parent();

		$('html, body').animate({
			scrollTop: parent.height()
		}, 1000);
	})

	function showPreview(projectPreview) {
		if(projectPreview.length > 0 ) {
			setTimeout(function(){
				projectPreview.addClass('bg-loaded');
				showPreview(projectPreview.next());
			}, 150);
		}
	}

	function setBodyHeight() {
		var selectedProject = projects.find('.selected'),
		previewImage = selectedProject.find('.preview-image'),
		projectInfo = selectedProject.find('.cd-project-info'),
		bodyHeight = previewImage.outerHeight() + projectInfo.outerHeight();
		
		projectInfo.height('toggle');

		console.log(projectInfo.height());

		$('[role=main]').height(bodyHeight);
	}

	function openProject(projectPreview) {
		var projectIndex = projectPreview.index();
		projects.children('li').eq(projectIndex).add(projectPreview).addClass('selected');

		if( transitionsNotSupported ) {
			projectPreviews.addClass('slide-out').removeClass('selected');
			projects.children('li').eq(projectIndex).addClass('content-visible');
			animating = false;
		} else { 
			slideToggleProjects(projectPreviews, projectIndex, 0, true);
		}

		setBodyHeight();
	}

	function closeProject() {
		projects.find('.selected').removeClass('selected').on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
			$(this).removeClass('content-visible').off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend');
			slideToggleProjects(projectsPreviewWrapper.children('li'), -1, 0, false);
		});
 
		if( transitionsNotSupported ) {
			projectPreviews.removeClass('slide-out');
			projects.find('.content-visible').removeClass('content-visible');
			animating = false;
		}
	}

	function slideToggleProjects(projectsPreviewWrapper, projectIndex, index, bool) {
		if(index == 0 ) createArrayRandom();
		if( projectIndex != -1 && index == 0 ) index = 1;

		var randomProjectIndex = makeUniqueRandom();
		if( randomProjectIndex == projectIndex ) randomProjectIndex = makeUniqueRandom();
		
		if( index < numRandoms - 1 ) {
			projectsPreviewWrapper.eq(randomProjectIndex).toggleClass('slide-out', bool);
			setTimeout( function(){ 
				slideToggleProjects(projectsPreviewWrapper, projectIndex, index + 1, bool);
			}, 150);
		} else if ( index == numRandoms - 1 ) { 
			projectsPreviewWrapper.eq(randomProjectIndex).toggleClass('slide-out', bool).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
				if( projectIndex != -1) {
					projects.children('li.selected').addClass('content-visible');
					projectsPreviewWrapper.eq(projectIndex).addClass('slide-out').removeClass('selected');
				} else if( navigation.hasClass('nav-visible') && bool ) {
					navigation.addClass('nav-clickable');
				}
				projectsPreviewWrapper.eq(randomProjectIndex).off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend');
				animating = false;
			});
		}
	}
 
	function makeUniqueRandom() {
	    var index = Math.floor(Math.random() * uniqueRandoms.length);
	    var val = uniqueRandoms[index]; 
	    uniqueRandoms.splice(index, 1);
	    return val;
	}

	function createArrayRandom() { 
		uniqueRandoms.length = 0;
		for (var i = 0; i < numRandoms; i++) {
            uniqueRandoms.push(i);
        }
	}
});

(function($){
 	$.fn.bgLoaded = function(custom) {
	 	var self = this;

		// Default plugin settings
		var defaults = {
			afterLoaded : function(){
				this.addClass('bg-loaded');
			}
		};

		// Merge default and user settings
		var settings = $.extend({}, defaults, custom);

		// Loop through element
		self.each(function(){
			var $this = $(this),
				bgImgs = $this.css('background-image').split(', ');
			$this.data('loaded-count',0);
			$.each( bgImgs, function(key, value){
				var img = value.replace(/^url\(["']?/, '').replace(/["']?\)$/, '');
				$('<img/>').attr('src', img).load(function() {
					$(this).remove(); // prevent memory leaks
					$this.data('loaded-count',$this.data('loaded-count')+1);
					if ($this.data('loaded-count') >= bgImgs.length) {
						settings.afterLoaded.call($this);
					}
				});
			});

		});
	};
})(jQuery);

(function($){
	$(document).ready(function(){
		$('div.header-spacer').height($('header.header').outerHeight());
		// SlickSlider
		$('.full-banner-carousel').slick({
			dots: false,
			arrows: false,
			autoplay: true,
			slidesToShow: 1,
			slidesToScroll: 1
		});

		$('.icon-carousel').on('init setPosition.fireOnce', function() {
			console.log('slider')
			$(window).trigger('sliderPos');
			$('.icon-carousel').off('.fireOnce');
		})

		$('.icon-carousel').slick({
			dots: false,
			arrows: false,
			autoplay: true,
			slidesToShow: 5,
			slidesToScroll: 1,
			responsive: [{
					breakpoint: 1200,
					settings: {
						slidesToShow: 4,
					}
				},{
					breakpoint: 992,
					settings: {	
						slidesToShow: 3,
					}
				},{
					breakpoint: 768,
					settings: {
						slidesToShow: 2,
					}
				},{
					breakpoint: 576,
					settings: {
						slidesToShow: 1,
					}
			}]
		});
		
		$('.icon-carousel-titled').slick({
			dots: false,
			arrows: false,
			autoplay: true,
			slidesToShow: 4,
			slidesToScroll: 1,
			responsive: [{
					breakpoint: 1200,
					settings: {
						slidesToShow: 4,
					}
				},{
					breakpoint: 992,
					settings: {	
						slidesToShow: 3,
					}
				},{
					breakpoint: 768,
					settings: {
						slidesToShow: 2,
					}
				},{
					breakpoint: 576,
					settings: {
						slidesToShow: 1,
					}
			}]
		});
	});
})(jQuery);
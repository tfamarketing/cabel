$(document).ready(function () {
    let $loadMorePosts = $('.tfa-load-more-posts');

    if ($loadMorePosts.length) {
        let windowKeys = Object.keys(window);
        $loadMorePosts.each(function () {
            let $this = $(this),
                idAction = $this.attr('id');

            if ($.inArray(idAction, windowKeys)) {
                let localizedData = window[idAction],
                    $btn = $this.find('.tfa-load-more-posts-btn'),
                    $result = $this.find('.tfa-load-more-posts-result'),
                    idActionClass = idAction + '_post',
                    startPage = parseFloat(localizedData.currentPage),
                    dataCache = {};

                $btn.click(function () {
                    var data = {
                        action: idAction,
                        currentPage: localizedData.currentPage,
                        maxPage: localizedData.maxPage,
                        posts: localizedData.posts,
                        atts: localizedData.atts
                    };

                    function appendPosts(data) {
                        if (data && localizedData.currentPage != localizedData.maxPage) {
                            $result.html(data).find('>*').addClass(idActionClass);
                            if (localizedData.atts.scrollToResults) {
                                $result.find('>*').first().addClass('scroll-to-this');
                            }
                            if (localizedData.atts.appendToParent) {
                                $result.find('>*').insertBefore($this);
                            }
                            if (localizedData.atts.scrollToResults) {
                                $("html, body").animate({
                                    scrollTop: $('.' + idActionClass + '.scroll-to-this').offset().top
                                }, 600);
                                $('.' + idActionClass + '.scroll-to-this').removeClass('scroll-to-this');
                            }
                            localizedData.currentPage++;
                        }
                        if (localizedData.currentPage == localizedData.maxPage) {
                            if (localizedData.atts.btnLessText) {
                                $btn.text(localizedData.atts.btnLessText);
                            } else {
                                $btn.remove();
                            }
                            $this.trigger('tfa_load_more_posts.posts_complete');
                        } else {
                            if (localizedData.atts.btnLessText) {
                                $btn.text(localizedData.atts.btnMoreText);
                            }
                            $this.trigger('tfa_load_more_posts.posts_appended');
                        }
                    }

                    if (localizedData.currentPage != localizedData.maxPage) {
                        if (!dataCache[parseFloat(localizedData.currentPage)]) {
                            $.post({
                                ...(localizedData.atts.preLoadText) ? {
                                    beforeSend: () => {
                                        $result.append('<p class="loading-more">' + localizedData.atts.preLoadText + '</p>');
                                    }
                                } : {},
                                ...{
                                    url: localizedData.ajaxUrl,
                                    data: data,
                                    success: (data) => {
                                        dataCache[localizedData.currentPage] = data;
                                        appendPosts(data);
                                    }
                                }
                            });
                        } else {
                            appendPosts(dataCache[localizedData.currentPage]);
                        }
                    } else {
                        $('.' + idActionClass).remove();
                        if (localizedData.atts.scrollToResults) {
                            $("html, body").animate({
                                scrollTop: $this.parent().find('>*').first().offset().top
                            }, 600);
                        }
                        if (localizedData.atts.btnLessText) {
                            $btn.text(localizedData.atts.btnMoreText);
                        }
                        localizedData.currentPage = startPage;
                    }
                })
            }
        });
    }
});
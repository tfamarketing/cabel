<?php
/*
 *  Author: William Roberts - TFA Marketing
 *  URL: tfamarketing.com | @tfamarketing
 *  Custom functions, support, custom post types and more.
 */

use Auryn\Injector;
use TFA\Init;
use TFA\Interfaces\Run;

// Adds composer autoload file
$autoLoad = locate_template('vendor/autoload.php', false, false);

if (!empty($autoLoad)) {
    include_once $autoLoad;
}

if (class_exists(Injector::class) && is_a(Init::class, Run::class, true)) {
    $injector = new Injector;
    $globalParams = (array) include_once 'inc/globals.php';
    $injector->define(Init::class, [$injector, $globalParams]);
    $init = $injector->make(Init::class);
    $injector->execute([$init, 'run']);
}

add_filter('show_admin_bar', '__return_false');

// Includes Component
function include_component($name, $content = '') {
    $content = $content;
    include get_template_directory().'/templates/components/'.$name.'.php';
}

// Checks Array For Specific Empty Fields
function checkEmptyArray(array $array, $fields, $harsh = false) {
    if(!is_array($fields)) $fields = [$fields];
    $checks = [];
    foreach($fields as $key => $field) { 
        if(is_array($field)) {
            foreach($field as $fiel) {
                $checks[$key.'---'.$fiel] = empty($array[$key][$fiel]);
            }
        } else {
            $checks[$field] = !empty($array[$field]);
        }
    }
    if($harsh) return !in_array(true, $checks);
    return in_array(false, $checks); 
} 
 
// Includes Custom Shortcodes
include_once get_template_directory().'/Shortcodes/shortcodes.php';
TFA\ShortCodes\ShortCodeBuilder::registerShortCodes();

// Font Colours
function my_mce4_options($init) { 
    $init['textcolor_map'] = '["FFFFFF", "White", "0A242C", "Black", "84C200", "Green"]'; 
    $init['textcolor_rows'] = 1;
    return $init;
}
add_filter('tiny_mce_before_init', 'my_mce4_options');

// Adds Icon Size 
add_image_size('icon', 50, 50);

// Adds Styling to Admin Area
function admin_styles() {
    echo '<style>';
    include get_template_directory().'/dist/admin.css';
    echo '</style>';
}
add_action('admin_head', 'admin_styles');
add_editor_style('/dist/admin.css');

// Re Add Underline to Tiny MCE
function ratb_tiny_mce_buttons_underline( $buttons_array ){
    if (!in_array('underline', $buttons_array)){ 
		array_splice( $buttons_array, 0, 0, ['underline'] );
	}
	return $buttons_array;
}
add_filter( 'mce_buttons_2', 'ratb_tiny_mce_buttons_underline', 5 );	

// Check if office open
function officeOpen($time = null){
    if($time === null) $time = time(); 
    $info = get_field('information', 'option');
    $open = false;
    switch(date('D', $time)) {
        case 'Mon':
            if($info['open_hours']['monday']['is_open'] && ($time >= strtotime($info['open_hours']['monday']['open']) && $time < strtotime($info['open_hours']['monday']['close']))) 
                $open = true; 
            break;
        case 'Tue':
            if($info['open_hours']['tuesday']['is_open'] && ($time >= strtotime($info['open_hours']['tuesday']['open']) && $time < strtotime($info['open_hours']['tuesday']['close']))) 
                $open = true;
            break;
        case 'Wed':
            if($info['open_hours']['wednesday']['is_open'] && ($time >= strtotime($info['open_hours']['wednesday']['open']) && $time < strtotime($info['open_hours']['wednesday']['close']))) 
                $open = true;
            break;
        case 'Thu':
            if($info['open_hours']['thursday']['is_open'] && ($time >= strtotime($info['open_hours']['thursday']['open']) && $time < strtotime($info['open_hours']['thursday']['close']))) 
                $open = true;
            break;
        case 'Fri':
            if($info['open_hours']['friday']['is_open'] && ($time >= strtotime($info['open_hours']['friday']['open']) && $time < strtotime($info['open_hours']['friday']['close']))) 
                $open = true;
            break;
        case 'Sat':
            if($info['open_hours']['saturday']['is_open'] && ($time >= strtotime($info['open_hours']['saturday']['open']) && $time < strtotime($info['open_hours']['saturday']['close']))) 
                $open = true;
            break;
        case 'Sun':
            if($info['open_hours']['sunday']['is_open'] && ($time >= strtotime($info['open_hours']['sunday']['open']) && $time < strtotime($info['open_hours']['sunday']['close']))) 
                $open = true;
            break;
    } 
    return $open;
}

// SVG decoration
function include_arrow($colour = 'white', $style = 'solid') {
    echo TFA\Helpers\Svg::inline(get_template_directory().'/img/arrows/green-hollow-arrow.svg', ['arrow-'.$colour.'-'.$style, 'svg-arrow']);
}
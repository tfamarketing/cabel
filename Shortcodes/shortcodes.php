<?php namespace TFA\ShortCodes;

class ShortCodeBuilder { 
    private $name;
    private $params = [];
    private $html;

    public static function registerShortCodes() {
        $files = preg_grep('/^([^.])/', scandir(get_template_directory()."/Shortcodes/Templates")); 
        foreach ($files as $filename) { 
            include_once get_template_directory().'/Shortcodes/Templates/'.$filename;
            new ShortCodeBuilder(str_replace('.php', '', $filename), $params, $html);
        }
    }

    public function __construct($name, $params, $html) {
        $this->name = $name;
        $this->params = $params;
        $this->html = $html;

        $this->addShortcode();
    }

    private function addShortcode() {
        add_shortcode($this->name, [$this, 'shortCode']);
    }

    public function shortCode($atts) {  
        $args = shortcode_atts( $this->params , $atts);
        if(isset($args['component']) && !empty($args['component'])) {
            include_component($args['component']);
        } else {
            echo $this->injectParams($args);
        }
    }

    private function injectParams($atts) { 
        foreach($atts as $key => $att) {
            $vars['{'.$key.'}'] = $att;
        } 

        return strtr($this->html, $vars);
    }
}